<?php

namespace KayStrobach\Sitemgr\Backend\Tree\View;


class BrowseTreeView extends \TYPO3\CMS\Backend\Tree\View\BrowseTreeView
{
    /**
     * @var
     */
    protected $dataIdentifier;

    /**
     * @return mixed
     */
    public function getDataIdentifier()
    {
        return $this->dataIdentifier;
    }

    /**
     * @param mixed $dataIdentifier
     */
    public function setDataIdentifier($dataIdentifier)
    {
        $this->dataIdentifier = $dataIdentifier;
    }

    function wrapIcon($icon,$row)   {
        return '<a href="#">' . $icon . '</a>';
    }
    function wrapTitle($title,$row,$bank=0)	{
        $aOnClick = '$(\'*[data-hiddenTreeField="' . $this->dataIdentifier . '"]\').val(\'' . $row['uid'] . '\');$(this).closest(".treeWrapper").find(".list-tree-group").removeClass("bg-success");$(this).closest(".list-tree-group").addClass("bg-success")';
        return '<a href="#" onclick="' . htmlspecialchars($aOnClick) . '">' . $title . '</a>';
    }
}