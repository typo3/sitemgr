<?php

namespace KayStrobach\Sitemgr\Command;

use KayStrobach\Sitemgr\Domain\Service\SiteConfigurationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FixDoublePrefixCommand extends Command
{
    protected SiteConfigurationService $siteConfigrationService;

    protected SiteFinder $siteFinder;

    public function __construct(
        string $name = null
    )
    {
        parent::__construct($name);
        $this->siteConfigrationService = GeneralUtility::makeInstance(SiteConfigurationService::class);
        $this->siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
    }

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Show entries from the sys_log database table of the last 24 hours.');
        $this->setHelp('Prints a list of recent sys_log entries.' . LF . 'If you want to get more detailed information, use the --verbose option.');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $sites = $this->siteFinder->getAllSites(false);

        foreach ($sites as $site) {
            $io->writeln('Processing: ' . $site->getIdentifier() . ' on UID ' . $site->getRootPageId());
            $subpages = $this->getSubPages($site->getRootPageId());
            // fix the root page

            $prefix = '/' . preg_replace('/[^a-z0-9\-]/', '', strtolower($site->getAttribute('websiteTitle')));

            $this->updateBrokenSlugs(
                $site->getRootPageId(),
                $prefix,
                '/'
            );
            // fix the subpages
            $this->updateBrokenSlugs(
                $subpages,
                $prefix . '/',
                '/'
            );
        }

        return 0;
    }

    protected function updateBrokenSlugs(string $subpages, string $prefix, string $replaceWith)
    {
        // might be faster with a single SQL Query, but that might break on some DBMS
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $stmt = $connectionPool
            ->getConnectionForTable('pages')
            ->prepare(
                'UPDATE pages SET slug = REPLACE(slug, ?, ?) WHERE uid in (' . $subpages . ') AND SUBSTR(slug, 1, ?) = ?'
            );
        $stmt->execute(
            [
                $prefix,
                $replaceWith,
                strlen($prefix),
                $prefix,
            ]
        );
    }

    protected function getSubPages(int $pid): string
    {
        $queryGenerator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( \TYPO3\CMS\Core\Database\QueryGenerator::class );
        return $queryGenerator->getTreeList($pid, 99999999, 0, 1); //Will be a string like 1,2,3
    }
}
