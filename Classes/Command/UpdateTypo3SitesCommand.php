<?php

namespace KayStrobach\Sitemgr\Command;

use KayStrobach\Sitemgr\Domain\Model\Customer;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\SiteConfigurationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UpdateTypo3SitesCommand extends Command
{
    protected SiteConfigurationService $siteConfigrationService;

    public function __construct(
        string $name = null
    )
    {
        parent::__construct($name);
        $this->siteConfigrationService = GeneralUtility::makeInstance(SiteConfigurationService::class);
    }

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Show entries from the sys_log database table of the last 24 hours.');
        $this->addArgument('templateFileForSites', InputArgument::REQUIRED, 'The file, which is included as a template in every new site');
        $this->addArgument('pid', InputArgument::REQUIRED, 'The PID of the site, where we want to have the sites below');
        $this->setHelp('Prints a list of recent sys_log entries.' . LF . 'If you want to get more detailed information, use the --verbose option.');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $subpages = $this->getSubPages($input->getArgument('pid'));

        $io->writeln('Found ' . count($subpages) . ' below site ' . $input->getArgument('pid'));

        foreach ($subpages as $subpage) {
            $io->writeln('Checking UID: ' . $subpage);
            if ($this->siteConfigrationService->pageIsSite($subpage)) {
                $io->error('  Page ' . $subpage . ' does already have a site config - skipping');
                $io->success('  Page ' . $subpage . ' will get an updated site config');
                $this->siteConfigrationService->createSiteConfigForCustomer(
                    $subpage,
                    $input->getArgument('templateFileForSites')
                );
                continue;
            }
            $io->success('  Page ' . $subpage . ' will get a new site config');

            $this->siteConfigrationService->createSiteConfigForCustomer(
                $subpage,
                $input->getArgument('templateFileForSites')
            );
        }
        return 0;
    }

    protected function getSubPages(int $pid): array
    {
        $queryGenerator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( \TYPO3\CMS\Core\Database\QueryGenerator::class );
        $childPids = $queryGenerator->getTreeList($pid, 1, 0, 1); //Will be a string like 1,2,3
        return explode(',',$childPids );
    }
}
