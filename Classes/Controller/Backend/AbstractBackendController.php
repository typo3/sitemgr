<?php

namespace KayStrobach\Sitemgr\Controller\Backend;


use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Lang\LanguageService;


abstract class AbstractBackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * Backend Template Container.
     * Takes care of outer "docheader" and other stuff this module is embedded in.
     *
     * @var string
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;

    /**
     * @var array|null
     */
    protected $menuItems = [
        [
            'controller' => 'Backend\Default',
            'action' => 'index',
            'label' => 'Default',
        ],
        [
            'controller' => 'Backend\Customer',
            'action' => 'index',
            'label' => 'Customers',
            'adminOnly' => true,
        ],
        [
            'controller' => 'Backend\BackendUser',
            'action' => 'index',
            'label' => 'BackendUsers'
        ],
        [
            'controller' => 'Backend\System',
            'action' => 'index',
            'label' => 'Systemstatus',
            'adminOnly' => true
        ]
    ];

    protected $buttons = [
        'save' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_common.xlf:saveAndClose',
            'showLabelText' => true,
            'icon' => 'actions-document-save-close'
        ],
        'addCustomer' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:db_new.php.pagetitle',
            'showLabelText' => true,
            'controller' => 'Backend\Customer',
            'action' => 'new',
            'icon' => 'actions-document-new'
        ],
        'exitCustomer' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_common.xlf:close',
            'controller' => 'Backend\Customer',
            'action' => 'index',
            'icon' => 'actions-close'
        ],
        'addUser' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:db_new.php.pagetitle',
            'showLabelText' => true,
            'controller' => 'Backend\BackendUser',
            'action' => 'new',
            'icon' => 'actions-document-new'
        ],
        'addAcl' => [
            'label' => 'LLL:EXT:be_acl/Resources/Private/Languages/locallang_perm.xml:addAcl',
            'showLabelText' => true,
            'controller' => 'Backend\Acl',
            'action' => 'new',
            'icon' => 'actions-document-new'
        ],
        'editAcl' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_common.xlf:close',
            'controller' => 'Backend\Acl',
            'action' => 'edit',
            'icon' => 'actions-close'
        ],
        'exitUser' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_common.xlf:close',
            'controller' => 'Backend\BackendUser',
            'action' => 'index',
            'icon' => 'actions-close'
        ]
    ];

    protected $enabledButtons = [];

    protected $id = null;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \TYPO3\CMS\Core\Domain\Repository\PageRepository
     */
    public $pageRepository;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository
     */
    public $customerRepository;

    public function injectPageRepositoryDependency(\TYPO3\CMS\Core\Domain\Repository\PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function injectCustomerRepositoryDependency(CustomerRepository $cr)
    {
        $this->customerRepository = $cr;
    }

    protected function initializeAction()
    {
        parent::initializeAction();
        if (isset($_GET['id'])) {
            $this->id = (int)$_GET['id'];
        }
    }

    /**
     * Initializes the current action
     *
     * @return void
     */
    protected function initializeView(ViewInterface $view)
    {
        if ($view instanceof BackendTemplateView) {
            if ($this->menuItems !== null) {
                $this->addMenuItems($this->menuItems, $view);
            }
            $this->view->getModuleTemplate()->getDocHeaderComponent()->setMetaInformation(
                $this->pageRepository->getPage($this->id)
            );
            $this->view->getModuleTemplate()->getPageRenderer()->addCssFile(
                PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('sitemgr')) . 'Resources/Public/Stylesheets/main.css'
            );
        }
    }

    /**
     *  Example Entry for an Menu Item
     *
     *  'index' => [
     *      'controller' => 'BackendUser',
     *      'action' => 'index',
     *      'label' => LLL:EXT:beuser/Resources/Private/Language/locallang.xml:backendUsers
     *  ],
     *
     * @param array $menuItems
     * @param ViewInterface $view
     */
    protected function addMenuItems($menuItems, $view = null)
    {
        if ($view === null) {
            $view = $this->view;
        }
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);

        $menu = $view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
        $menu->setIdentifier('BackendUserModuleMenu');

        foreach ($menuItems as  $menuItemConfig) {
            $isActive = false;
            if (isset($menuItemConfig['adminOnly']) && ($menuItemConfig['adminOnly'] === true) && ($this->getBackendUserAuthentication()->isAdmin() !== true)) {
                continue;
            }
            if ($this->request->getControllerName() === $menuItemConfig['controller']) {
                $isActive = $this->request->getControllerActionName() === $menuItemConfig['action'] ? true : false;
            }
            if ($this->id !== null) {
                $menuItem = $menu->makeMenuItem()
                    ->setTitle($this->getLanguageService()->sL($menuItemConfig['label']))
                    ->setHref($this->getHref($menuItemConfig['controller'], $menuItemConfig['action']))
                    ->setActive($isActive);
                $menu->addMenuItem($menuItem);
            }
        }

        $view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->addMenu($menu);
    }

    protected function enableButton($name, $parameters = null)
    {
        if (!isset($this->buttons[$name])) {
            return;
        }
        /** @var ButtonBar $buttonBar */
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();

        if (isset($this->buttons[$name]['controller'])) {
            $button = $buttonBar->makeLinkButton()
                ->setHref($this->getHref($this->buttons[$name]['controller'], $this->buttons[$name]['action'], $parameters))
                ->setShowLabelText(isset($this->buttons[$name]['showLabelText']) ? true : false)
                ->setTitle($this->getLanguageService()->sL($this->buttons[$name]['label']))
                ->setIcon($this->view->getModuleTemplate()->getIconFactory()->getIcon($this->buttons[$name]['icon'], Icon::SIZE_SMALL));
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        } else {
            $button = $buttonBar->makeInputButton()
                ->setForm('EditDocumentController')
                ->setShowLabelText(isset($this->buttons[$name]['showLabelText']) ? true : false)
                ->setTitle($this->getLanguageService()->sL($this->buttons[$name]['label']))
                ->setIcon($this->view->getModuleTemplate()->getIconFactory()->getIcon($this->buttons[$name]['icon'], Icon::SIZE_SMALL))
                ->setName('_savedok')
                ->setValue(1);
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @return \TYPO3\CMS\Core\Localization\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * Creates te URI for a backend action
     *
     * @param string $controller
     * @param string $action
     * @param array $parameters
     * @return string
     */
    protected function getHref($controller, $action, $parameters = [])
    {
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $uriBuilder->setTargetPageUid($this->id);
        return $uriBuilder->reset()->uriFor($action, $parameters, $controller);
    }
}
