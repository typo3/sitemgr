<?php

namespace KayStrobach\Sitemgr\Controller\Backend;


use KayStrobach\Sitemgr\Domain\Service\AclService;
use KayStrobach\Sitemgr\Domain\Service\CustomerService;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Domain\Model\BackendUser;

class AclController extends AbstractBackendController
{
    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository
     */
    public $customerRepository;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Service\CustomerService
     */
    public $customerService;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Service\AclService
     */
    public $aclService;

    public function __construct()
    {
        $this->customerService = new CustomerService();
        $this->aclService = new AclService();
    }

    protected function initializeAction()
    {
        parent::initializeAction();
        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);
        // avoid non administrative access to controller
        if (($customer === null) || (!$this->customerService->isUserAdministratorOfCustomer($customer, $this->getBackendUserAuthentication()))) {
            $this->redirect(
                'index',
                'Backend\Default'
            );
        }
    }

    public function indexAction()
    {
        $this->enableButton('exitUser');
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\BackendUser $backendUser
     * @return void
     * @throws \Exception
     */
    public function editAction(\TYPO3\CMS\Extbase\Domain\Model\BackendUser $backendUser)
    {
        $this->enableButton('exitUser');
        $this->enableButton(
            'addAcl',
            [
                'backendUser' => $backendUser
            ]
        );

        $this->view->assign('user', $backendUser);
        $this->view->assign('pagePermissions', $this->aclService->getPagePermissions($backendUser->getUid()));
        $this->view->assign('currentUser', $this->getBackendUserAuthentication());

        $this->view->assign('customer', $this->customerRepository->findCustomerForPageRecursive($this->id));
    }

    /**
     * @param BackendUser $backendUser
     * @param int $uid
     */
    public function removeAction(BackendUser $backendUser, $uid)
    {
        $this->view->assign('backendUser', $backendUser);
        $this->view->assign('uid', $uid);
        $this->aclService->removePagePermission(
            $uid,
            $backendUser->getUid()
        );
        $this->redirect(
            'edit',
            null,
            null,
            [
                'backendUser' => $backendUser
            ]
        );
    }

    public function newAction(\TYPO3\CMS\Extbase\Domain\Model\BackendUser $backendUser)
    {
        $this->enableButton(
            'editAcl',
            [
                'backendUser' => $backendUser
            ]
        );
        $this->enableButton('save');
        $this->view->assign('user', $backendUser);
        $this->view->assign('pagePermissions', $this->aclService->getPagePermissions($backendUser->getUid()));
        $this->view->assign('currentUser', $this->getBackendUserAuthentication());

        $this->view->assign('customer', $this->customerRepository->findCustomerForPageRecursive($this->id));
    }

    /**
     * @param BackendUser $backendUser
     * @param int $uid
     */
    public function createAction(BackendUser $backendUser, $uid)
    {
        $currentCustomer = $this->customerRepository->findCustomerForPageRecursive($this->id);
        $newPermCustomer = $this->customerRepository->findCustomerForPageRecursive($uid);

        if ($newPermCustomer->getUid() !== $currentCustomer->getUid()) {
            $this->addFlashMessage(
                'You are not allowed to add permissions for that page',
                '',
                AbstractMessage::ERROR
            );
            $this->redirect(
                'edit',
                null,
                null,
                [
                    'backendUser' => $backendUser
                ]
            );
        }

        $this->view->assign('backendUser', $backendUser);
        $this->view->assign('uid', $uid);
        try {
            $this->aclService->addPagePermission(
                $uid,
                $backendUser->getUid()
            );
        } catch (\Exception $e) {
            $this->addFlashMessage(
                'There was a Problem creating the ACL: ' . $e->getMessage(),
                'No ACL created',
                AbstractMessage::ERROR
            );
        }

        $this->redirect(
            'edit',
            null,
            null,
            [
                'backendUser' => $backendUser
            ]
        );
    }
}
