<?php

namespace KayStrobach\Sitemgr\Controller\Backend;

use KayStrobach\Sitemgr\Domain\Service\CustomerService;
use KayStrobach\Sitemgr\Utilities\FormEngineUtility;
use TYPO3\CMS\Backend\Form\Exception\AccessDeniedTableModifyException;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Domain\Model\BackendUser;

class BackendUserController extends AbstractBackendController
{
    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Utilities\FormEngineUtility
     */
    public $formEngineUtility;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Service\CustomerService
     */
    public $customerService;

    /**
     * @var int
     */
    protected $backendUserIsAdmin = 0;

    public function __construct()
    {
        $this->customerService = new CustomerService();
    }

    public function injectFormEngineUtilityDependency(FormEngineUtility $formEngineUtility)
    {
        $this->formEngineUtility = $formEngineUtility;
    }


    protected function initializeAction()
    {
        parent::initializeAction();
        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);
        // avoid non administrative access to controller
        if (($customer === null) || (!$this->customerService->isUserAdministratorOfCustomer($customer, $this->getBackendUserAuthentication()))) {
            $this->redirect(
                'index',
                'Backend\Default'
            );
        }
    }

    protected function makeAdmin()
    {
        $this->backendUserIsAdmin = $this->getBackendUserAuthentication()->user['admin'];
        $this->getBackendUserAuthentication()->user['admin'] = 1;
    }

    protected function restoreAdmin()
    {
        $this->getBackendUserAuthentication()->user['admin'] = $this->backendUserIsAdmin;
    }

    public function indexAction()
    {
        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);
        $this->enableButton('addUser');
        $this->view->assign('customer', $customer);
        $this->view->assign('currentUserIsAdmin', $this->getBackendUserAuthentication()->isAdmin());
    }

    protected function renderUserForm(BackendUser $backendUser, $fields = null)
    {
        if ($fields === null) {
            $fields = [
                'avatar',
                'description',
                'disable',
                'email',
                'endtime',
                'starttime',
                'lastlogin',
                'realName',
                'username'
            ];
        }

        $body = $this->formEngineUtility->renderForm(
            'be_users',
            (int)$backendUser->getUid(),
            'edit',
            $this->formEngineUtility->filterFields(
                $GLOBALS['TCA']['be_users'],
                $fields
            )
        );
        return $body;
    }

    public function editAction(BackendUser $backendUser = null, int $backendUserUid = null)
    {
        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);

        if ($backendUserUid !== null) {
            $backendUser = $customer->getBackendUserByUid($backendUserUid);
        }
        if ($backendUser === null) {
            $this->redirect(
                'index',
                null,
                null,
                null,
                $this->id
            );
        }

        if (!$customer->isAllowedToManageUser($backendUser->getUid())) {
            $this->redirect(
                'index',
                null,
                null,
                null,
                $this->id
            );
        }

        $this->makeAdmin();
        $this->enableButton('exitUser');
        $this->enableButton('save');

        $body = $this->renderUserForm(
            $backendUser
        );
        $this->restoreAdmin();

        $this->view->assign('form', $body);
        $this->view->assign('user', $backendUser);
    }

    public function editPasswordAction(BackendUser $backendUser)
    {
        $this->makeAdmin();
        $this->enableButton('exitUser');
        $this->enableButton('save');

        $body = $this->renderUserForm(
            $backendUser,
            [
                'password'
            ]
        );
        $this->restoreAdmin();

        $this->view->assign('form', $body);
        $this->view->assign('user', $backendUser);
    }

    /**
     *
     */
    public function updateAction(BackendUser $backendUser = null)
    {
        if (!isset($_POST['data'])) {
            $this->addFlashMessage('data missing');
            $this->redirect('index');
        }
        $data = $_POST['data'];

        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);

        foreach ($data['be_users'] as $uid => $user) {
            if (!$customer->isAllowedToManageUser($uid)) {
                throw new AccessDeniedTableModifyException('You are not allowed to modify ' . $uid);
            }
        }

        $this->formEngineUtility->handleData(
            $data,
            [],
            [],
            true
        );

        $this->redirect('index');
    }

    public function newAction()
    {
        $this->enableButton('exitUser');
        $this->enableButton('save');

        $this->makeAdmin();
        $body = $this->formEngineUtility->renderForm(
            'be_users',
            0,
            'new',
            null,
            null,
            'username,password,avatar,realName,lang,starttime,endtime,description',
            (int)$_GET['id']
        );
        $this->restoreAdmin();

        $this->view->assign('form', $body);
    }

    public function createAction()
    {
        if (!isset($_POST['data'])) {
            $this->addFlashMessage('data missing');
            $this->redirect('index');
        }
        $data = $_POST['data'];

        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);
        if ($customer === null) {
            $this->redirect('index');
        }

        foreach ($data['be_users'] as $beUser) {
            $this->customerService->createBackendUserForCustomer(
                $customer,
                $beUser
            );
        }


        $this->redirect('index');
    }
}
