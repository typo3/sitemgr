<?php

namespace KayStrobach\Sitemgr\Controller\Backend;

use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\CustomerService;
use KayStrobach\Sitemgr\Utilities\SystemCheckUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class DefaultController extends AbstractBackendController
{
    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Utilities\SystemCheckUtility
     */
    public $systemCheckUtility;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository
     */
    public $customerRepository;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Domain\Service\CustomerService
     */
    public $customerService;

    public function __construct()
    {
        $this->customerService = new CustomerService();
    }

    public function injectSystemCheckDependency(SystemCheckUtility $su)
    {
        $this->systemCheckUtility = $su;
    }

    /**
     * Displays all Rooms
     *
     * @return string The rendered list view
     */
    public function indexAction()
    {
        if ($this->systemCheckUtility->hasErrors()) {
            $this->redirect(
                'index',
                'Backend\System'
            );
        }

        $customer = $this->customerRepository->findCustomerForPageRecursive($this->id);

        if ($this->getBackendUserAuthentication()->isAdmin()) {
            if ($customer === null) {
                $this->redirect(
                    'index',
                    'Backend\Customer'
                );
            } else {
                $this->redirect(
                    'index',
                    'Backend\BackendUser',
                    null,
                    [
                        'customer' => $customer
                    ],
                    $customer->getUid()
                );
            }
        }

        if ($customer !== null) {
            if ($this->customerService->isUserAdministratorOfCustomer($customer, $this->getBackendUserAuthentication())) {
                $this->redirect(
                    'index',
                    'Backend\BackendUser',
                    null,
                    [
                        'customer' => $customer
                    ],
                    $customer->getUid()
                );
            }
        }

        $this->view->assign('customer', $customer);
    }
}
