<?php

namespace KayStrobach\Sitemgr\Controller\Backend;


use KayStrobach\Sitemgr\Utilities\SystemCheckUtility;

class SystemController extends AbstractBackendController
{
    /**
     * @TYPO3\CMS\Extbase\Annotation\Inject
     * @var \KayStrobach\Sitemgr\Utilities\SystemCheckUtility
     */
    public $systemCheckUtility;

    public function injectSystemCheckDependency(SystemCheckUtility $su)
    {
        $this->systemCheckUtility = $su;
    }

    /**
     * Displays all Rooms
     *
     * @return string The rendered list view
     */
    public function indexAction()
    {
        $this->view->assign('checks', $this->systemCheckUtility->getCheckResults());
        $this->view->assign('currentUser', $this->getBackendUserAuthentication());
    }
}
