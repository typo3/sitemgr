<?php

namespace KayStrobach\Sitemgr\Domain\Model;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Domain\Model\BackendUser;
use TYPO3\CMS\Extbase\Domain\Model\BackendUserGroup;
use TYPO3\CMS\Extbase\Domain\Repository\BackendUserRepository;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class Customer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    protected string $title = '';

    protected string $alias = '';

    /**
     * @var BackendUser
     */
    protected $mainBeUser = null;

    /**
     * @var ObjectStorage<BackendUser>
     */
    protected $adminBeUsers;

    /**
     * @var ObjectStorage<BackendUser>
     */
    protected $normalBeUsers;

    /**
     * @var ObjectStorage<BackendUserGroup>
     */
    protected $beGroups;

    /**
     * @var bool
     */
    protected $isSiteroot;

    /**
     * @var PersistenceManager
     */
    protected $persistenceManager;

    public function __construct()
    {
        $this->adminBeUsers = new ObjectStorage();
        $this->normalBeUsers = new ObjectStorage();
        $this->beGroups = new ObjectStorage();
        $this->persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return BackendUser
     */
    public function getMainBeUser()
    {
        return $this->mainBeUser;
    }

    /**
     * @param BackendUser $mainBeUser
     */
    public function setMainBeUser(BackendUser $mainBeUser)
    {
        $this->mainBeUser = $mainBeUser;
    }

    /**
     * @return ObjectStorage<BackendUser>
     */
    public function getAdminBeUsers()
    {
        return $this->adminBeUsers;
    }

    /**
     * @param ObjectStorage<BackendUser> $adminBeUsers
     */
    public function setAdminBeUsers(ObjectStorage $adminBeUsers)
    {
        $this->adminBeUsers = $adminBeUsers;
    }

    /**
     * @return ObjectStorage<BackendUser>
     */
    public function getNormalBeUsers()
    {
        return $this->normalBeUsers;
    }

    public function getNormalBeUsersCommaSeparated() {
        if ($this->getNormalBeUsers() instanceof \Iterator) {
            $users = [];
            /** @var BackendUser $user */
            foreach ($this->getNormalBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
            return implode(',', array_keys($users));
        }
        return '';
    }

    /**
     * @param ObjectStorage<BackendUser> $normalBeUsers
     */
    public function setNormalBeUsers(ObjectStorage $normalBeUsers)
    {
        $this->normalBeUsers = $normalBeUsers;
    }

    public function getAllBeUsers() {
        $users = [];
        if ($this->getMainBeUser() !== null) {
            $users[$this->getMainBeUser()->getUid()] = $this->getMainBeUser();
        }
        if ($this->getAdminBeUsers() instanceof \Iterator) {
            /** @var BackendUser $user */
            foreach($this->getAdminBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
        }
        if ($this->getNormalBeUsers() instanceof \Iterator) {
            /** @var BackendUser $user */
            foreach ($this->getNormalBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
        }
        return $users;
    }

    /**
     * @param $uid
     * @return bool
     */
    public function isAllowedToManageUser($uid)
    {
        $user = $this->getBackendUserByUid($uid);
        if ($user instanceof BackendUser) {
            return true;
        }
        return false;
    }

    /**
     * @return ObjectStorage
     */
    public function getBeGroups()
    {
        return $this->beGroups;
    }

    public function getBeGroupsCommaSeparated()
    {
        $groups = [];
        /** @var BackendUserGroup $group */
        foreach ($this->beGroups as $group) {
            $groups[] = $group->getUid();
        }
        return implode(',', $groups);
    }

    /**
     * @param ObjectStorage $beGroups
     */
    public function setBeGroups(ObjectStorage $beGroups)
    {
        $this->beGroups = $beGroups;
    }

    public function getRow()
    {
        return BackendUtility::getRecord(
            'pages',
            $this->getUid()
        );
    }

    /**
     * @return bool
     */
    public function isSiteroot(): bool
    {
        return $this->isSiteroot ?? false;
    }

    /**
     * @param bool $isSiteroot
     */
    public function setIsSiteroot(bool $isSiteroot): void
    {
        $this->isSiteroot = $isSiteroot;
    }

    public function getAllUsersIncludingDisabled(): QueryResultInterface
    {
        $customerRecord = BackendUtility::getRecord('pages', $this->getUid());
        $usersUids = [];
        $userObjects = [];
        $usersUids[] = $customerRecord['main_be_user'];
        $newUsers = explode(',', $customerRecord['admin_be_users']);
        if (count($newUsers) > 0) {
            array_push($usersUids, ...$newUsers);
        }
        $newUsers = explode(',', $customerRecord['normal_be_users']);
        if (count($newUsers) > 0) {
            array_push($usersUids, ...$newUsers);
        }
        $this->persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        $q = $this->persistenceManager->createQueryForType(BackendUser::class);
        $q->getQuerySettings()->setEnableFieldsToBeIgnored(['disable', 'starttime', 'endtime']);
        $q->getQuerySettings()->setIgnoreEnableFields(true);

        return $q->matching(
            $q->in('uid', $usersUids)
        )->execute();
    }

    public function getBackendUserByUid($uid): ?BackendUser
    {
        $users = $this->getAllUsersIncludingDisabled();
        foreach ($users as $user) {
            if (($user instanceof BackendUser) && $user->getUid() === $uid) {
                return $user;
            }
        }
        return null;
    }
}
