<?php

namespace KayStrobach\Sitemgr\Domain\Service;

use KayStrobach\Sitemgr\Domain\Model\Customer;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\QueryHelper;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AclService
{
    protected function getConnectionPool(string $table, bool $withoutDeletedRestriction): QueryBuilder
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $queryBuilder
            ->getRestrictions()
            ->removeAll();
        if ($withoutDeletedRestriction) {
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        }
        return $queryBuilder;
    }

    public function getPagePermissions($backendUser)
    {
        $user = BackendUtility::getRecord(
            'be_users',
            (int)$backendUser
        );
        $queryBuilder = $this->getConnectionPool('tx_beacl_acl', true);
        $queryBuilder->select('*')->from('tx_beacl_acl')->where($queryBuilder->expr()->eq('object_id', $queryBuilder->createNamedParameter((int)$backendUser)));
        $queryBuilder->andWhere(QueryHelper::stripLogicalOperatorPrefix('AND type="0"'));

        $grants   = $queryBuilder->execute()->fetchAll();

        $return = array();
        foreach($grants as $grant) {
            $path     = BackendUtility::getRecordPath(
                $grant['pid'],
                '',
                100
            );
            $return[$grant['pid']] = array(
                'userName' => $user['username'],
                'path'     => $path,
                'uid'      => $grant['uid'],
                'right'    => 'R/W',
                'pid'      => $grant['pid'],
            );
        }
        return $return;
    }

    public function addPagePermission($pid, $backendUser)
    {
        $customer = $this->getCustomerService()->getCustomerForPage($pid);

        if (!$customer instanceof Customer) {
            return false;
        }

        if(!$customer->isAllowedToManageUser($backendUser)) {
            return false;
        }
        //add mountpoint
        $user = BackendUtility::getRecord(
            'be_users',
            $backendUser
        );
        $user['db_mountpoints'] = GeneralUtility::uniqueList(
            $user['db_mountpoints'] . ',' . (int)$pid
        );

        $queryBuilder = $this->getConnectionPool('be_users', true);
        $queryBuilder->getConnection()->update(
            'be_users',
            $user,
            ['uid' => (int)$backendUser]
        );

        $now = time();

        /* @var $queryBuilder QueryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_beacl_acl');
        $queryBuilder
            ->getRestrictions()
            ->removeAll();

        $queryBuilder->insert(
            'tx_beacl_acl',
        )->values(
            [
                'pid'         => (int)$pid,
                'tstamp'      => $now,
                'crdate'      => $now,
                'cruser_id'   => $this->getBeUser()->user['uid'],
                'type'        => 0, //user
                'object_id'   => $backendUser,
                'permissions' => 25,
                'recursive'   => 0
            ]
        )->execute();

        $queryBuilder->insert(
            'tx_beacl_acl',
        )->values(
            [
                'pid'         => $pid,
                'tstamp'      => $now,
                'crdate'      => $now,
                'cruser_id'   => $this->getBeUser()->user['uid'],
                'type'        => 0, //user
                'object_id'   => $backendUser,
                'permissions' => 31,
                'recursive'   => 1
            ]
        )->execute();
        return true;
    }

    public function removePagePermission($pid, $backendUser)
    {
        $customer = $this->getCustomerService()->getCustomerForPage($pid);

        if (!$customer instanceof Customer) {
            return false;
        }

        if(!$customer->isAllowedToManageUser($backendUser)) {
            return false;
        }

        //drop acls
        $queryBuilder = $this->getConnectionPool('be_users', true);
        $queryBuilder->getConnection()->delete(
            'tx_beacl_acl',
            [
                'pid' => (int)$pid,
                'object_id' => (int)$backendUser,
                'type' => 0
            ]
        );


        //drop mountpoints
        $user = BackendUtility::getRecord(
            'be_users',
            (int)$backendUser
        );
        $user['db_mountpoints'] = GeneralUtility::rmFromList(
            $pid,
            $user['db_mountpoints']
        );

        $queryBuilder = $this->getConnectionPool('be_users', true);
        $queryBuilder->getConnection()->update(
            'be_users',
            $user,
            ['uid' => (int)$backendUser]
        );
    }

    /**
     * @return CustomerService
     */
    public function getCustomerService()
    {
        return GeneralUtility::makeInstance(CustomerService::class);
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
