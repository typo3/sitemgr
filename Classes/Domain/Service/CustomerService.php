<?php

namespace KayStrobach\Sitemgr\Domain\Service;


use KayStrobach\Sitemgr\Domain\Model\Customer;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\Exception\InvalidFormDataException;
use KayStrobach\Sitemgr\Domain\Service\Exception\SettingMissingException;
use KayStrobach\Sitemgr\Utilities\FileSystemUtility;
use KayStrobach\Sitemgr\Utilities\FormEngineUtility;
use KayStrobach\Sitemgr\Utilities\PageTsConfigUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\BackendUser;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class CustomerService implements SingletonInterface
{
    /**
     * @var \KayStrobach\Sitemgr\Utilities\FormEngineUtility
     */
    protected $formEngineUtility;

    public function __construct()
    {
        $this->formEngineUtility = GeneralUtility::makeInstance(FormEngineUtility::class);
    }

    public function isUserAdministratorOfCustomer(Customer $customer, BackendUserAuthentication $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
        if ($user->check('custom_options', 'tx_sitemgr_custom:permissions')) {
            return true;
        }

        if (($customer->getMainBeUser() !== null) && ($customer->getMainBeUser()->getUid() === $user->user['uid'])) {
            return true;
        }
        /** @var BackendUser $customerUser */
        if ($customer->getAdminBeUsers() instanceof \Iterator) {
            foreach ($customer->getAdminBeUsers() as $customerUser) {
                if ($customerUser->getUid() === $user->user['uid']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function isUserOfCustomer(Customer $customer, BackendUserAuthentication $user)
    {
        if ($this->isUserAdministrator($customer, $user)) {
            return true;
        }
        /** @var BackendUser $customerUser */
        foreach ($customer->getNormalBeUsers() as $customerUser) {
            if ($customerUser->getUid() === $user->user['uid']) {
                return true;
            }
        }
        return false;
    }

    public function createBackendUserForCustomer(Customer $customer, $arg)
    {
        $extConfig = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('sitemgr');
        if ($extConfig['forceBeUserPrefix'] && (strpos($arg['username'], $customer->getTitle()) !== 0)) {
            $arg['username'] = $customer->getTitle() . '-' . $arg['username'];
        }

        $tce = $this->formEngineUtility->handleData(
            [
                'be_users' => [
                    'NEW39459' => [
                        'pid' => 0,
                        'username' => $arg['username'],
                        'usergroup' => $customer->getBeGroupsCommaSeparated(),
                        'disable' => 0,
                        'options' => 2
                    ]
                ]
            ],
            [],
            [],
            true
        );

        $beUserUid = $tce->substNEWwithIDs['NEW39459'];

        $conn = $this->getConnectionPool('pages', true);
        $conn->update('pages')
            ->where($conn->expr()->eq('uid', $customer->getUid()))
            ->set('normal_be_users', $customer->getNormalBeUsersCommaSeparated() . ',' . $beUserUid)
            ->execute();
    }

    protected function getConnectionPool(string $table, bool $withoutDeletedRestriction): QueryBuilder
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $queryBuilder
            ->getRestrictions()
            ->removeAll();
        if ($withoutDeletedRestriction) {
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        }
        return $queryBuilder;
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

    public function createCustomer($arg)
    {
        /***********************************************************************
         * fetch needed options
         */
        $tgroup = PageTsConfigUtility::getForCurrentUserAndUid($arg['pid'])['mod.']['web_txsitemgr.']['customer.']['createUser.']['group.'] ?? null;
        if (trim($tgroup['value']) === '') {
            throw new SettingMissingException(
                'Sry, but you need to define mod.web_txsitemgr.customer.createUser.group in PageTS to ensure proper user rights' . print_r($arg,
                    true),
                1491762898
            );
        }

        /***********************************************************************
         * create first step records
         */
        //------------------------------------------------------------------
        //be_groups & be_users
        $data = array(
            //create page
            'pages' => array(
                'NEW11' => array(
                    'pid' => $arg['pid'],
                    'doktype' => 4,
                    'title' => $arg['title'],
                    'nav_title' => $arg['title'],
                    'description' => $arg['description'],
                    'hidden' => 0,
                    'shortcut_mode' => 1,
                    'alias' => $arg['title'],
                    'editlock' => 1,
                ),
                //create dummy page
                'NEW13' => array(
                    'pid' => 'NEW11',
                    'hidden' => '0',
                    'title' => 'Start'
                ),
            ),
            //create group
            'be_groups' => array(
                'NEW41' => array(
                    'pid' => 0,
                    'title' => 'E: ' . $arg['title'],
                    'hidden' => 0,
                    'subgroup' => $tgroup['value'],
                    'db_mountpoints' => 'NEW11',
                ),
            ),
            //create user
            'be_users' => array(
                'NEW31' => array(
                    'pid' => 0,
                    'username' => $arg['title'],
                    'realName' => $arg['title'] . '-admin',
                    'email' => $arg['customerEmail'],
                    'password' => $arg['password'],
                    'usergroup' => 'NEW41',
                    'fileoper_perms' => 15,
                    'lang' => $this->getBeUser()->uc['lang'],
                    // set this user lang as default language for the new user
                    'options' => 2,
                    'db_mountpoints' => 'NEW11',
                ),
            ),
        );

        //execute hook
        $this->executeDatabasePreprocessing(
            'customerCreateRound2',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'defaultLang' => $this->getBeUser()->uc['lang'],
            )
        );


        /** @var DataHandler $tcemain */
        $tcemain = GeneralUtility::makeInstance(DataHandler::class);
        $tcemain->start(
            $data,
            array(),
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        $groupId = $tcemain->substNEWwithIDs['NEW41'];
        $userId = $tcemain->substNEWwithIDs['NEW31'];
        $pageId = $tcemain->substNEWwithIDs['NEW11'];

        /***********************************************************************
         * create second step records
         */
        $data = array(
            //create template
            'sys_template' => array(
                'NEW21' => array(
                    'pid' => $pageId,
                    'constants' => '######################################################################' . PHP_EOL .
                        '# EXT:ks_sitemgr' . PHP_EOL .
                        '# createdate: ' . date('r') . PHP_EOL .
                        '# userfolder: ' . $GLOBALS['$TYPO3_CONF_VARS']['BE']['userHomePath'] . $userId . PHP_EOL .
                        '  usr_name                    = ' . $userId . PHP_EOL .
                        '  usr_root                    = ' . $pageId . PHP_EOL .
                        '  plugin.tx_sitemgr.username  = ' . $arg['title'] . PHP_EOL .
                        '  plugin.tx_sitemgr.useremail = ' . $arg['customerEmail'] . PHP_EOL .
                        '  plugin.tx_sitemgr.userId    = ' . $userId . PHP_EOL .
                        '  plugin.tx_sitemgr.rootPage  = ' . $pageId . PHP_EOL .
                        '######################################################################' . PHP_EOL,
                    'sitetitle' => $arg['customerName'],
                    'title' => 'template for ext:sitemgr, contains username const. only',
                    'root' => 1,
                ),
            ),
            //create customer
            'tx_sitemgr_customer' => array(
                'NEW61' => array(
                    'pid' => $pageId,
                    'title' => $arg['title'],
                    'main_be_user' => $userId,
                    'be_groups' => $groupId,
                ),
            ),
            //create acl
            'tx_beacl_acl' => array(
                'NEW51' => array(
                    'pid' => $pageId,
                    'type' => 0,
                    'object_id' => $userId,
                    'cruser_id' => $userId,   //set creator to owner
                    'permissions' => 25,       //do not delete rootpage, but allow all other things execpt edit
                    'recursive' => 0,
                ),
                'NEW52' => array(
                    'pid' => $pageId,
                    'type' => 0,
                    'object_id' => $userId,   //allow all for subpages
                    'cruser_id' => $userId,   //set creator to owner
                    'permissions' => 31,
                    'recursive' => 1,
                ),
            ),
            //modify be user
            'be_users' => array(
                $userId => array(
                    'db_mountpoints' => $pageId,
                    'password' => $arg['password'],
                ),
            ),
        );

        //execute hook
        $this->executeDatabasePreprocessing(
            'customerCreateRound2',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'defaultLang' => $this->getBeUser()->uc['lang'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'customerAdminUid' => $userId,
                'customerGroupUid' => $groupId,
                'customerRootPid' => $pageId,
            )
        );

        $tcemain = GeneralUtility::makeInstance(DataHandler::class);
        $tcemain->start(
            $data,
            array(),
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        /***********************************************************************
         * Fix problem with updating password
         */
        $data['be_users'][$userId]['password'] = md5($arg['password']);
        $conn = $this->getConnectionPool('be_users');
        $conn->update('be_users')
            ->set('be_users', $userId)
            ->where($conn->expr()->eq('uid', $userId))
            ->execute();
        /***********************************************************************
         * create user and group folder
         */

        $this->ensureUserHomeFolderExists($userId);
        $this->ensureGroupHomeFolderExists($groupId);

        /***********************************************************************
         * clear cache
         */
        if ($arg['copyPagesFrom'] == 'on') {
            $tcemain = GeneralUtility::makeInstance(DataHandler::class);
            $tcemain->copyTree = 99;
            $tcemain->copyWhichTables = '*';
            $cmd = array(
                'pages' => array(
                    $arg['copyPagesFrom'] => array(
                        'copy' => $pageId
                    ),
                ),
            );
            $tcemain->start(
                array(),
                $cmd,
                $this->getBackendUserAsAdmin()
            );
            $tcemain->process_cmdmap();
        }

        $this->executeDatabasePreprocessing(
            'customerCreateRound3',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'defaultLang' => $this->getBeUser()->uc['lang'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'customerAdminUid' => $userId,
                'customerGroupUid' => $groupId,
                'customerRootPid' => $pageId,
            )
        );

        /***********************************************************************
         * clear cache
         */
        $tcemain->clear_cacheCmd('pages');
    }

    /**
     * @param string $hookname
     * @param array $fields
     * @param array $params
     */
    private function executeDatabasePreprocessing($hookname, &$fields, $params)
    {
        /** @var FileSystemUtility $fileSystemUtility */
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['sitemgr'][$hookname])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['sitemgr'][$hookname] as $userFunc) {
                GeneralUtility::callUserFunction($userFunc, $fields, $params, $this);
            }
        }
    }

    public function ensureUserHomeFolderExists($userId)
    {
        /** @var FileSystemUtility $fileSystemUtility */
        $fileSystemUtility = GeneralUtility::makeInstance(FileSystemUtility::class);
        if (trim($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']) !== '') {
            try {
                $fileSystemUtility->ensureFolderExists(rtrim($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'],
                        '/') . '/' . $userId);
            } catch (ExistingTargetFolderException $e) {
                // well this is fine
            }

        }
    }

    public function ensureGroupHomeFolderExists(int $groupId)
    {
        $fileSystemUtility = GeneralUtility::makeInstance(FileSystemUtility::class);
        if (trim($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']) !== '') {
            try {
                $fileSystemUtility->ensureFolderExists(
                    rtrim($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath'], '/') . '/' . $groupId
                );
            } catch (ExistingTargetFolderException $e) {
                // well this is fine
            }
        }
    }

    public function getCustomerForPage(int $uid): Customer
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $objectManager->get(CustomerRepository::class);
        return $customerRepository->findCustomerForPageRecursive($uid);
    }

    protected function getBackendUserAsAdmin(): BackendUserAuthentication
    {
        $user = clone $this->getBeUser();
        $user->user['admin'] = 1;
        return $user;
    }
}
