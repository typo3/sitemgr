<?php

namespace KayStrobach\Sitemgr\Domain\Service;

use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class SiteConfigurationService
{
    protected SiteConfiguration $siteConfiguration;

    protected SiteFinder $siteFinder;

    protected CustomerRepository $customerRepository;

    public function __construct()
    {
        $this->siteConfiguration = $siteConfiguration ?? GeneralUtility::makeInstance(
                SiteConfiguration::class,
                Environment::getConfigPath() . '/sites'
            );
        $this->siteFinder = $siteFinder ?? GeneralUtility::makeInstance(
            SiteFinder::class
        );

        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->customerRepository = $objectManager->get(CustomerRepository::class);

        $this->siteFinder->getAllSites();
    }

    public function pageIsSite(int $uid) {
        try {
            $this->siteFinder->getSiteByRootPageId($uid);
            return true;
        } catch (SiteNotFoundException $e) {
            return false;
        }
    }

    public function createSiteConfigForCustomer(int $uid, string $templateFileForSites)
    {
        $customer = $this->customerRepository->findCustomerForPage($uid);
        if ($customer === null) {
            return;
        }

        $this->siteConfiguration->write(
            '_' . $uid,
            $this->buildYamlCodeForSite(
                $templateFileForSites,
                $uid
            )
        );

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll();
        $queryBuilder
            ->update('pages')
            ->where($queryBuilder->expr()->eq('uid', $uid))
            ->set('is_siteroot', 1)
            ->set('doktype',  CustomerRepository::CUSTOMER_DOCTYPE)
            ->execute()
        ;
    }

    protected function buildYamlCodeForSite(string $templateFileForSites, int $rootPage)
    {
        $loader = GeneralUtility::makeInstance(YamlFileLoader::class);
        $baseConfig = $loader->load(
            // GeneralUtility::fixWindowsFilePath($fileName), 0)
            // baseFile aus den ExtensionSettings,
            'EXT:sitemgr/Configuration/Defaults/sites/base.yaml',
            0
        );

        $customer = $this->customerRepository->findCustomerForPage($rootPage);

        $pageTitle = $customer->getTitle();
        $pathSegment = preg_replace('/[^a-z0-9\-]/', '', strtolower($pageTitle));

        array_walk_recursive(
            $baseConfig,
            static function (&$item) use ($pathSegment) {
                $item = str_replace(
                    '%replace(customer)%',
                    $pathSegment,
                    $item
                );
            }
        );

        $code = [
            'imports' => [
                [
                    'resource' => $templateFileForSites,
                ],
            ],
            'errorHandling' => [
                [
                    'errorCode' => '404',
                    'errorHandler' => 'Page',
                    'errorContentSource' => 't3://page?uid=' . $rootPage
                ]
            ],
            'websiteTitle' => $pageTitle,
            'rootPageId' => $rootPage,
        ];

        ArrayUtility::mergeRecursiveWithOverrule($code, $baseConfig);

        return $code;
    }
}
