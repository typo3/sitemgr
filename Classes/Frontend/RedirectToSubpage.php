<?php

namespace KayStrobach\Sitemgr\Frontend;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\ViewHelpers\Uri\TypolinkViewHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;

class RedirectToSubpage
{
    /**
     *
     * @param  string $content Empty string (no content to process)
     * @param  array  $conf    TypoScript configuration
     * @return string          HTML output, showing the current server time.
     */
    public function redirect($content, $conf)
    {
        $firstSubPage = $this->getFirstSubpage($this->getTSFE()->id);
        if ($firstSubPage !== null) {
            $uri = $this->getUri($firstSubPage);
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . (string)$uri);
        }
        return '';
    }

    protected function getFirstSubpage($pid)
    {
        /** @var \KayStrobach\Sitemgr\Frontend\PageRepository $pageRepository */
        $pageRepository = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Domain\Repository\PageRepository::class);
        $pages = $pageRepository->getMenu(
            (int)$pid,
            '*',
            'sorting',
            'doktype < 200',
        );
        if ($pages === null || (count($pages) === 0)) {
            return null;
        }
        foreach ($pages as $page) {
            if ($page['doktype'] < 200) {
                return $page['uid'];
            }
        }
        return null;
    }

    /**
     * @return \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }

    protected function getUri($parameter, $additionalParams = [], $useCacheHash = true)
    {
        /** @var ContentObjectRenderer $contentObject */
        $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $uri = $contentObject->typoLink_URL(
            [
                'parameter' => self::createTypolinkParameterFromArguments($parameter, $additionalParams),
                'forceAbsoluteUrl' => true,
                'useCacheHash' => $useCacheHash,
            ]
        );

        return $uri;
    }

    /**
     * Transforms ViewHelper arguments to typo3link.parameters.typoscript option as array.
     *
     * @param string $parameter Example: 19 _blank - "testtitle with whitespace" &X=y
     * @param string $additionalParameters
     *
     * @return string The final TypoLink string
     */
    protected static function createTypolinkParameterFromArguments($parameter, $additionalParameters = '')
    {
        $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
        $typolinkConfiguration = $typoLinkCodec->decode($parameter);

        // Combine additionalParams
        if ($additionalParameters) {
            $typolinkConfiguration['additionalParams'] .= $additionalParameters;
        }

        return $typoLinkCodec->encode($typolinkConfiguration);
    }
}
