<?php

namespace KayStrobach\Sitemgr\Hooks;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

abstract class AbstractDataHandlerHook
{
    public const GROUP_PREFIX = 'C: ';

    /**
     * @param $data
     * @return DataHandler
     */
    protected function processDatabaseOperations($data, $pObj)
    {
        if ($pObj instanceof DataHandler) {
            $tcemain = $pObj;
        } else {
            /** @var DataHandler $tcemain */
            $tcemain = GeneralUtility::makeInstance(DataHandler::class);
        }

        $tcemain->start(
            $data,
            [],
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        return $tcemain;
    }

    protected function getBackendUserAsAdmin(): BackendUserAuthentication
    {
        $user = clone $this->getBeUser();
        $user->user['admin'] = 1;
        return $user;
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
