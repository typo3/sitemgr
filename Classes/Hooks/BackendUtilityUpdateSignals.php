<?php

namespace KayStrobach\Sitemgr\Hooks;


use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class BackendUtilityUpdateSignals
{
    public function updateModule(&$params, $ref)
    {
        $moduleUrl = GeneralUtility::makeInstance(UriBuilder::class)->buildUriFromRoute('web_SitemgrTxSitemgrMod1', ['id' => $params['parameter']['id']]);
        $params['JScode'] = '
            if (typeof top.TYPO3.Backend !== undefined) {
                top.jump("' . $moduleUrl . '", "web_SitemgrTxSitemgrMod1", "web", ' . $params['parameter']['id'] . ')
            }
        ';
    }

    public function startCustomerEdit(&$params, $ref)
    {
        $id = (int)$params['parameter']['id'];
        $uriParameters = [
            'edit' => [
                'pages' => [
                    $id => 'edit'
                ]
            ],
            'id' => $id,
            'returnUrl' => GeneralUtility::makeInstance(UriBuilder::class)->buildUriFromRoute('web_SitemgrTxSitemgrMod1', ['id' => $id])
        ];

        $moduleUrl = GeneralUtility::makeInstance(UriBuilder::class)->buildUriFromRoute('record_edit', $uriParameters);
        $params['JScode'] = '
            if (typeof top.TYPO3.Backend !== undefined) {
                top.jump("' . $moduleUrl . '", "web_SitemgrTxSitemgrMod1", "web", ' . $id . ')
            }
        ';
    }
}
