<?php

namespace KayStrobach\Sitemgr\Hooks;


use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\CustomerService;
use KayStrobach\Sitemgr\Domain\Service\SiteConfigurationService;
use KayStrobach\Sitemgr\Utilities\PageTsConfigUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Configuration\ExtensionConfiguration;

class DataHandlerCmdHook extends AbstractDataHandlerHook implements SingletonInterface
{

    public function __construct()
    {
    }

    public function processCmdmap_afterFinish(DataHandler $dataHandler)
    {
        if (is_array($dataHandler->cmdmap['pages'])) {
            foreach ($dataHandler->cmdmap['pages'] as $uid => $cmdArray) {
                if (!isset($cmdArray['copy'])) {
                    continue;
                }
                // fetch the ID of the new customer!
                $newUid = $dataHandler->copyMappingArray['pages'][$uid];
                $pageRow = BackendUtility::getRecord('pages', $newUid, 'doktype,title,pid');
                if (($pageRow !== null) && ($pageRow['doktype'] === CustomerRepository::CUSTOMER_DOCTYPE)) {
                    $this->handleNewEntry(
                        'pages',
                        $newUid,
                        $pageRow,
                        $dataHandler
                    );
                }
            }
        }
    }

    public function handleNewEntry($table, $id, $fieldArray, $pObj)
    {
        if (isset($pObj->substNEWwithIDs[$id])) {
            $id = $pObj->substNEWwithIDs[$id];
        }

        $this->cleanUpAclAndTemplate($id);

        $this->createUserAndGroup($id, $fieldArray, $pObj);

        $groupUid = $pObj->substNEWwithIDs['NEW41'];
        $userUid = $pObj->substNEWwithIDs['NEW31'];
        $this->setUpPageAclAndTemplate(
            $id,
            $userUid,
            $groupUid,
            $pObj
        );

        /** @var CustomerService $customerService */
        $customerService = GeneralUtility::makeInstance(CustomerService::class);
        $customerService->ensureUserHomeFolderExists($userUid);
        $customerService->ensureGroupHomeFolderExists($groupUid);

        /** @var ExtensionConfiguration $sitemgrConfiguration */
        $defaultSiteConfig = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('sitemgr', 'defaultSiteTemplate');

        /** @var SiteConfigurationService $siteConfigrationService */
        $siteConfigrationService = GeneralUtility::makeInstance(SiteConfigurationService::class);
        $siteConfigrationService->createSiteConfigForCustomer($id, $defaultSiteConfig);

        BackendUtility::setUpdateSignal('SiteMgr::startCustomerEdit', ['id' => $id]);
    }

    protected function createUserAndGroup($id, $fieldArray, $pObj)
    {
        $defaultGroup = PageTsConfigUtility::getForCurrentUserAndUid($id)['mod.']['web_txkssitemgrM1.']['createUser.']['group'] ?? null;

        $data = [
            'be_groups' => [
                'NEW41' => [
                    'pid' => 0,
                    'title' => self::GROUP_PREFIX . $fieldArray['title'] . ' (pid=' . (int)$id . ')',
                    'hidden' => 0,
                    'subgroup' => $defaultGroup['value'],
                    'db_mountpoints' => $id,
                ],
            ],
            //create user
            'be_users' => [
                'NEW31' => [
                    'pid' => 0,
                    'disable' => 0,
                    'username' => $fieldArray['title'],
                    'realName' => $fieldArray['title'] . '-administrator',
                    'email' => '',
                    'password' => '',
                    'usergroup' => 'NEW41',
                    'fileoper_perms' => 15,
                    'lang' => $this->getBeUser()->uc['lang'],
                    // set this user lang as default language for the new user
                    'options' => 2,
                    'db_mountpoints' => $id,
                ],
            ],
        ];
        $this->processDatabaseOperations($data, $pObj);
    }

    protected function setUpPageAclAndTemplate($id, $userUid, $groupUid, $pObj)
    {
        $data = [
            'pages' => [
                $id => [
                    'main_be_user' => $userUid,
                    'normal_be_users' => '',
                    'be_groups' => $groupUid,
                    'hidden' => 0,
                ]
            ],
            'tx_beacl_acl' => [
                'NEW51' => [
                    'pid' => $id,
                    'type' => 0,
                    'object_id' => $userUid,
                    'cruser_id' => $userUid,   //set creator to owner
                    'permissions' => 25,       //do not delete rootpage, but allow all other things
                    'recursive' => 0,
                ],
                'NEW52' => [
                    'pid' => $id,
                    'type' => 0,
                    'object_id' => $userUid,   //allow all for subpages
                    'cruser_id' => $userUid,   //set creator to owner
                    'permissions' => 31,
                    'recursive' => 1,
                ],
            ],
            'sys_template' => [
                'NEW66' => [
                    'pid' => $id,
                    'title' => 'Customer Template: ' . $id,
                    'root' => 1,
                    'constants' => implode(
                        PHP_EOL,
                        [
                            '######################################################################',
                            '# EXT:sitemgr',
                            '# userfolder: ' . $userUid,
                            'usr_name                    = ' . $userUid,
                            'usr_root                    = ' . $id,
                            'plugin.tx_sitemgr.userId    = ' . $userUid,
                            'plugin.tx_sitemgr.rootPage  = ' . $groupUid,
                            '######################################################################',
                        ]
                    )
                ]
            ]
        ];
        $this->processDatabaseOperations($data, $pObj);
    }

    protected function cleanUpAclAndTemplate($id)
    {
        /** @var QueryBuilder $q */
        $q = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_template');
        $q->update('sys_template')
            ->where(
                $q->expr()->eq('pid', $id)
            )
            ->set('deleted', 1)
            ->execute();

        /** @var QueryBuilder $q */
        $q = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_beacl_acl');
        $q->delete('tx_beacl_acl')
            ->where(
                $q->expr()->eq('pid', $id)
            )
            ->execute();
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
