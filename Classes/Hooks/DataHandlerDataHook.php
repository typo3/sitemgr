<?php

namespace KayStrobach\Sitemgr\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;

class DataHandlerDataHook extends  AbstractDataHandlerHook implements SingletonInterface
{
    /**
     * @param string $status (new or edit)
     * @param string $table
     * @param int $id
     * @param array $fieldArray
     * @param DataHandler $pObj
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $pObj)
    {
        if ($table === 'pages') {
            if ($status !== 'new') {
                $this->handleEditEntry($table, $id, $fieldArray, $pObj);
            }
        }
    }

    protected function handleEditEntry($table, $id, $fieldArray, $pObj)
    {
        if (!isset($fieldArray['title'])) {
            return;
        }

        $pageEntry = BackendUtility::getRecord(
            'pages',
            $id,
            'title,main_be_user,admin_be_users,normal_be_users,be_groups'
        );

        #if ($fieldArray['title'] === $pageEntry['title']) {
        #    return;
        #}

        $data = [];

        foreach (explode(',', $pageEntry['be_groups']) as $group) {
            $data['be_groups'][$group]['title'] = DataHandlerCmdHook::GROUP_PREFIX . $fieldArray['title'] . ' (pid=' . (int)$id . ')';
        }

        if ((isset($pageEntry['main_be_user'])) && (strlen($pageEntry['main_be_user']) > 0)) {
            $data['be_users'][$pageEntry['main_be_user']]['username'] = $fieldArray['title'];
        }
        $this->processDatabaseOperations($data, $pObj);
        BackendUtility::setUpdateSignal('SiteMgr::startCustomerEdit', ['id' => $id]);
    }
}
