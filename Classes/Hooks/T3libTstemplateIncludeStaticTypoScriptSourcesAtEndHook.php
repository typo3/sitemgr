<?php

namespace KayStrobach\Sitemgr\Hooks;

// class for: $TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSourcesAtEnd'][]
use KayStrobach\Sitemgr\Domain\Model\Customer;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class T3libTstemplateIncludeStaticTypoScriptSourcesAtEndHook
{
    protected static $processed = [];

    /**
     * Includes static template records (from static_template table) and static template files (from extensions) for the input template record row.
     *
     * @param array \Array of parameters from the parent class. Includes idList, templateId, pid, and row.
     * @param TemplateService $pObj
     * @return void
     */
    public static function main(&$params, TemplateService &$pObj)
    {
        $idList = $params['idList'];
        $templateId = $params['templateId'];

        if ($templateId === $idList) {
            self::$processed[$templateId] = true;
            // @codingStandardsIgnoreStart
            $themeItem = [
                'constants'           => self::getConstants($params, $pObj),
                'config'              => '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:sitemgr/Configuration/TypoScript/Redirect/setup.txt">',
                'title'               => 'sitemgr:' . 'sitemgr',
                'uid'                 => 'sitemgrmagic' . $params['row']['pid'],
            ];

            // @codingStandardsIgnoreEnd
            $pObj->processTemplate(
                $themeItem,
                $params['idList'] . ',ext_sitemgrmagic',
                $params['pid'],
                'sitemgrmagic' . $params['row']['pid'],
                $templateId
            );
        }
        return;
    }

    /**
     * @param $params
     * @return string
     */
    public static function getConstants(&$params, TemplateService &$pObj)
    {
        $pageUid = $params['row']['pid'];

        /** @var \KayStrobach\Sitemgr\Hooks\PageRepository $sys_page */
        $sys_page = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Domain\Repository\PageRepository::class);
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '')->get();

        $i = count($rootLine) - 1;
        $customer = null;
        do {
            if ($rootLine[$i]['doktype'] === CustomerRepository::CUSTOMER_DOCTYPE) {
                $customer = $rootLine[$i];
            }
            $i--;
        } while ($customer == null && isset($rootLine[$i]));

        if ($customer === null) {
            return '# no customer found' . PHP_EOL;
        }
        $constants = [];
        $constants[] = '# customer found (' . $customer['uid'] . ')';
        $constants[] = 'plugin.sitemgr.customer.title = ' . $customer['title'];
        $constants[] = 'plugin.sitemgr.customer.uid = ' . $customer['uid'];
        $constants[] = 'plugin.sitemgr.customer.groupHomePath = ' . (string)$GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath'] . $customer['uid'] . '/';

        return implode(PHP_EOL, $constants);
    }


}
