<?php
namespace KayStrobach\Sitemgr\Tca;

use GeorgRinger\News\Hooks\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Backend;

class RestrictFeUserGroupsItemProcFunc
{
    /**
     * Add two items to existing ones
     *
     * @param array $params
     */
    public function itemsProcFunc(array &$params): void
    {
        throw new \Exception(json_encode($params['items']));
        $pagesWithRecords = $tshis->getSubPages(0);

        // get SiteID similar to FeUsers
        // get all pages
        // get records

        # $res = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows( 'uid,title', 'fe_groups', "pid IN(".join(',', $pagesWithRecords).") AND deleted='0' AND hidden='0'", '', 'title' );
        # foreach ($res as $item) {
        #     $params['items'][] = array($item['title'], $item['uid']);
        # }

        $params['items'] = [];
    }

    // finally apply to all tables, with fe_groups
}
