<?php

namespace KayStrobach\Sitemgr\ToolbarItems\CustomerSelector;

use TYPO3\CMS\Backend\Controller\BackendController;
use TYPO3\CMS\Backend\Toolbar\ToolbarItemInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidExtensionNameException;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Lang\LanguageService;

class ToolbarItem implements ToolbarItemInterface
{
    /**
     * reference back to the backend object
     *
     * @var    PageRenderer
     */
    protected $backendReference;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * @var string
     */
    protected $EXTKEY = 'sitemgr';

    /**
     * ToolbarItem constructor.
     */
    public function __construct()
    {
        // @todo migrate JS to pagerenderer
        // $this->getPageRenderer()->loadRequireJsModule('TYPO3/CMS/Backend/LiveSearch');
        $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $this->backendReference = $this->getPageRenderer();
    }

    /**
     * checks whether the user has access to this toolbar item
     *
     * @return  boolean  true if user has access, false if not
     */
    public function checkAccess()
    {
        return $this->getBeUser()->user['admin'];
    }

    /**
     * returns additional attributes for the list item in the toolbar
     *
     * @return    array        list item HTML attibutes
     */
    public function getAdditionalAttributes()
    {
        return [
            'class' => 'tx-sitemgr-menu'
        ];
    }

    /**
     * adds the neccessary javascript to the backend
     *
     * @return    void
     */
    protected function addJavascriptToBackend()
    {
        $this->backendReference->addJsFooterFile(PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->EXTKEY)) . 'Resources/Public/JavaScripts/Modules/BeUser/beuserStore.js');
        $this->backendReference->addJsFooterFile(PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->EXTKEY)) . 'Resources/Public/JavaScripts/Modules/Customer/customerStore.js');
        $this->backendReference->addJsFooterFile(PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->EXTKEY)) . 'Resources/Public/JavaScripts/ToolbarItems/CustomerSelector.js');
    }

    /**
     * adds the neccessary CSS to the backend
     *
     * @return    void
     */
    protected function addCssToBackend()
    {
        $this->backendReference->addCssFile(
            'sitemgr',
            PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->EXTKEY)) . 'Resources/Public/Stylesheets/BackendMod1/main.css'
        );
        $this->backendReference->addCssFile(
            'sitemgr',
            PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->EXTKEY)) . 'Resources/Public/Stylesheets/ToolbarItems/CustomerSelector.css'
        );
    }

    /**
     * @todo check that function and remove dead code
     *
     * Render "item" part of this toolbar
     *
     * @return string Toolbar item HTML
     */
    public function getItem()
    {
        $view = $this->getFluidTemplateObject('CustomerToolbarItem.html');
        $view->assignMultiple(
            [
                'icon' => $this->iconFactory->getIcon('sitemgr')
            ]
        );

        return $view->render();
        $this->addJavascriptToBackend();
        $this->addCssToBackend();

        //@todo check
        $this->backendReference->addInlineLanguageLabelFile('EXT:' . $this->EXTKEY . '/Resources/Private/Language/Modules/BeUser/locallang.xml');
        $this->backendReference->addInlineLanguageLabelFile('EXT:' . $this->EXTKEY . '/Resources/Private/Language/Modules/Customer/locallang.xml');

        $buffer = '<a href="#" class="toolbar-item"><img src="' . PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath('sitemgr')) . 'icon_tx_sitemgr_customer.gif" class="t3-icon" style="background-image:none;"></a>';
        return $buffer;
    }

    /**
     * TRUE if this toolbar item has a collapsible drop down
     *
     * @return bool
     */
    public function hasDropDown()
    {
        return true;
    }

    /**
     * Render "drop down" part of this toolbar
     *
     * @return string Drop down HTML
     */
    public function getDropDown()
    {
        $view = $this->getFluidTemplateObject('CustomerToolbarItemDropDown.html');
        $view->assignMultiple(
            [

            ]
        );

        return $view->render();
    }

    /**
     * Returns an integer between 0 and 100 to determine
     * the position of this item relative to others
     *
     * By default, extensions should return 50 to be sorted between main core
     * items and other items that should be on the very right.
     *
     * @return int 0 .. 100
     */
    public function getIndex()
    {
        return 50;
    }

    /**
     * Returns current PageRenderer
     *
     * @return PageRenderer
     */
    protected function getPageRenderer():PageRenderer
    {
        return GeneralUtility::makeInstance(PageRenderer::class);
    }

    protected function getLanguageService():LanguageService
    {
        return $GLOBALS['LANG'];
    }

    /**
     * Returns a new standalone view, shorthand function
     *
     * @param string $filename Which templateFile should be used.
     *
     * @return StandaloneView
     * @throws InvalidExtensionNameException
     */
    protected function getFluidTemplateObject(string $filename):StandaloneView
    {
        /** @var StandaloneView $view */
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->setLayoutRootPaths([GeneralUtility::getFileAbsFileName('EXT:sitemgr/Resources/Private/Layouts')]);
        $view->setPartialRootPaths([GeneralUtility::getFileAbsFileName('EXT:sitemgr/Resources/Private/Partials/ToolbarItems')]);
        $view->setTemplateRootPaths([GeneralUtility::getFileAbsFileName('EXT:sitemgr/Resources/Private/Templates/ToolbarItems')]);

        $view->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName('EXT:sitemgr/Resources/Private/Templates/ToolbarItems/' . $filename));

        $view->getRequest()->setControllerExtensionName('Backend');
        return $view;
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
