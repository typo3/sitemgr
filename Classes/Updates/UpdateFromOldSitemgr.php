<?php

namespace KayStrobach\Sitemgr\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class UpdateFromOldSitemgr implements UpgradeWizardInterface
{
    protected $table = 'tx_sitemgr_customer';

    /**
     * @return string Unique identifier of this updater
     */
    public function getIdentifier(): string
    {
        return 'sitemgrUpdateFromOldSitemgr';
    }

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return 'moves content from sitemgr table into pages table';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'moves content from sitemgr table into pages table';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from($this->table)
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $result) {
            if ((int)$result['pid'] !== 0) {
                $connection->update(
                    'pages',
                    [
                        'doktype' => CustomerRepository::CUSTOMER_DOCTYPE,
                        'main_be_user' => $result['main_be_user'],
                        'admin_be_users' => $result['admin_be_users'],
                        'normal_be_users' => $result['normal_be_users'],
                        'be_groups' => $result['be_groups']
                    ],
                    [
                        'uid' => $result['pid']
                    ]
                );
            }
        }
        return true;
    }

    public function updateNecessary(): bool
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        return $connection->getDriver()->getSchemaManager($connection)->tablesExist([$this->table]);
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }
}
