<?php

namespace KayStrobach\Sitemgr\Utilities;

use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use PDO;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

class CustomerUtilities
{
    var $customerId = null;
    var $customer = array();
    var $cache = array();
    private $throwException = false;

    protected function getConnectionPool(string $table, bool $withDeletedRestriction): QueryBuilder
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $queryBuilder
            ->getRestrictions()
            ->removeAll();
        if ($withDeletedRestriction) {
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        }
        return $queryBuilder;
    }

    /**
     * @param int $customerId
     */
    function __construct($customerId = null)
    {
        $this->customerId = $customerId;
        if ($this->customerId !== null) {
            try {
                $this->init();
            } catch (\Exception $e) {
                // just hope for manual init ;)
            }
        }
    }

    /**
     * @throws \Exception
     * @return array
     */
    function init()
    {
        if ($this->customerId === null) {
            throw new \Exception ('no customer id set');
        }
        return $this->customer = BackendUtility::getRecord('tx_sitemgr_customer', $this->customerId);
    }

    function enableExceptions()
    {
        $this->throwException = true;
    }

    function disableExceptions()
    {
        $this->throwException = false;
    }

    /**
     * @throws \Exception
     * @param int $pageId
     * @return int
     */
    function getCustomerForPage($pageId)
    {
        if ($this->customerId !== null) {
            return $this->customerId;
        }
        $sys_page = GeneralUtility::makeInstance(PageRepository::class);
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageId, '')->get();
        $i = count($rootLine) - 1;
        $uid = null;
        do {
            $querybuilder = $this->getConnectionPool('tx_sitemgr_customer');
            $querybuilder->select('*')
                ->from('tx_sitemgr_customer')
                ->where(
                    $querybuilder->expr()->eq('pid', (int)$rootLine[$i]['uid'])
                );
            $stmt = $querybuilder->execute();
            if ($stmt->rowCount() === 1) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $uid = $row['uid'];
                $this->customer = $row;
            }
            $i--;
        } while ($uid === null && isset($rootLine[$i]));
        if ($uid === null || $uid === 0) {
            throw new \Exception('no customer found, object is not valid ...');
        }
        $this->customerId = $uid;
        return $uid;
    }

    function getCustomerForUser($uid)
    {
        $qb = $this->getConnectionPool('tx_sitemgr_customer', false);
        $qb->select('*')
            ->from('tx_sitemgr_customer')
            ->where(
                // condition
            );
        $main_be_user = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            '*',
            'tx_sitemgr_customer',
            'deleted = 0 AND ' . $GLOBALS['TYPO3_DB']->listQuery('main_be_user', $uid, 'tx_sitemgr_customer')
        );
        $admin_be_users = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            '*',
            'tx_sitemgr_customer',
            'deleted = 0 AND ' . $GLOBALS['TYPO3_DB']->listQuery('admin_be_users', $uid, 'tx_sitemgr_customer')
        );
        $normal_be_users = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            '*',
            'tx_sitemgr_customer',
            'deleted = 0 AND ' . $GLOBALS['TYPO3_DB']->listQuery('normal_be_users', $uid, 'tx_sitemgr_customer')
        );
        $customers = array();
        foreach ($main_be_user as $customer) {
            $customers[$customer['uid']] = $customer;
        }
        foreach ($admin_be_users as $customer) {
            $customers[$customer['uid']] = $customer;
        }
        foreach ($normal_be_users as $customer) {
            $customers[$customer['uid']] = $customer;
        }
        return $customers;
    }

    function getCustomerForUserAsString($uid): string
    {
        $buffer = array();
        foreach ($this->getCustomerForUser($uid) as $customer) {
            $buffer[] = $customer['title'];
        }
        return implode(',', $buffer);
    }

    function getPage()
    {
        $this->init();
        return $this->customer['pid'];
    }

    function getRootPage()
    {
        return $this->getPage();
    }

    function getName()
    {
        if ($this->customerId === null) {
            throw new \Exception('No Customer ID set, please call getCustomerPerPage before ...');
        }
        $customerId = $this->customerId;
        $erg = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            'title',
            'tx_sitemgr_customer',
            'uid=' . intval($customerId) . ' AND deleted=0'
        );
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($erg) !== 1) {
            throw new \Exception('0 or more than 1 customer found');
        }
        $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($erg);

        return $row['title'];
    }

    function getFolder()
    {
        if (GeneralUtility::isAllowedAbsPath($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'])) {
            return $GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'] . $this->getMainUserUid() . '/';
        }
        throw new \Exception('userHomePath is not allowed in localconf.php');
    }

    //----------------------------------------------------------------------
    function getMainUserUid()
    {
        return $this->customer['main_be_user'];
    }

    function getMainUser()
    {
        $users[] = $this->getMainUserUid();
        return $this->getCompleteUserData($users, 'getMainUser');
    }

    function getMainUserName()
    {
        $user = $this->getMainUser();
        $user = current($user);
        return $user['username'];
    }

    //----------------------------------------------------------------------
    function getAdminUsersUids()
    {
        $adminUsers[] = $this->getMainUserUid();
        $adminUsers = array_merge($adminUsers, explode(',', $this->customer['admin_be_users']));
        return $this->cleanArray($adminUsers);
    }

    function getAdminUsers()
    {
        $users = $this->getAdminUsersUids();
        return $this->getCompleteUserData($users, 'getAdminUsers');
    }

    //----------------------------------------------------------------------
    function getNormalUsersUids()
    {
        $users = explode(',', $this->customer['normal_be_users']);
        return $this->cleanArray($users);
    }

    function getNormalUsers()
    {
        $users = $this->getNormalUsersUids();
        return $this->getCompleteUserData($users, 'getNormalUsers');
    }

    //----------------------------------------------------------------------
    function getAllUsersUids()
    {
        $users = array_merge($this->getAdminUsersUids(), $this->getNormalUsersUids());
        return $this->cleanArray($users);
    }

    function getAllUsers()
    {
        $users = $this->getAllUsersUids();
        return $this->getCompleteUserData($users, 'getAllUsers');
    }

    //----------------------------------------------------------------------
    function getGroups()
    {
        return $this->customer['be_groups'];
        #$groups = explode(',',$this->customer['be_groups']);
        #return $groups;
    }

    //----------------------------------------------------------------------
    protected function getCompleteUserData($uidsArray, $cacheParam)
    {
        if (isset($this->cache['user->' . $cacheParam])) {
            return $this->cache['user->' . $cacheParam];
        }
        if (count($uidsArray) > 0) {
            $beFunc = GeneralUtility::makeInstance('\TYPO3\CMS\Backend\Utility\BackendUtility');
            $where = '';
            foreach ($uidsArray as $uid) {
                $where .= ' OR uid = ' . intval($uid);
            }
            //remove trailing or)
            $where = substr($where, 3);
            //add and for full condition...
            $where = 'AND (' . $where . ')';
            #$content = $where.$content;
            $users = $beFunc->getUserNames('realName,username,usergroup,usergroup_cached_list,uid,disable,admin,email', $where);
            $this->cache['user->' . $cacheParam] = $users;
            return $this->cache['user->' . $cacheParam];
        } else {
            return array();
        }
    }

    //----------------------------------------------------------------------
    protected function cleanArray($array)
    {
        $return = array();
        foreach ($array as $entry) {
            if ($entry) $return[] = $entry;
        }
        return $return;
    }

    //----------------------------------------------------------------------
    function isCustomerUser($uid)
    {
        return in_array($uid, $this->getAllUsersUids(), true);
    }

    function isAllowedToModifyUser($uid)
    {
        //do not edit your self
        if ($this->getBeUser()->user['uid'] === $uid) {
            return false;
        }
        //global admin?
        if ($this->getBeUser()->isAdmin()) {
            return true;
        }
        //is customer admin and uid in getAllUsersUid
        #print_r($this->getAdminUsersUids());
        #print_r($this->$this->getAllUsersUids());
        #die();
        if (in_array($this->getBeUser()->user['uid'], $this->getAdminUsersUids(), true)) {
            if (in_array($uid, $this->getAllUsersUids(), true)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @throws \Exception
     * @param integer|null $beUserId
     * @return bool
     */
    function isAdministratorForCustomer($beUserId = null)
    {
        if ($beUserId === null) {
            $beUserId = $this->getBeUser()->user['uid'];
            if ($this->getBeUser()->isAdmin()) {
                return true;
            }
        } else {
            $t = BackendUtility::getRecord('be_users', $beUserId);
            if (is_array($t)) {
                if ($t['admin'] === 1) {
                    return true;
                }
            }
        }
        if (in_array($beUserId, $this->getAdminUsersUids(), true)) {
            return true;
        }
        if ($this->throwException) {
            throw new \Exception('Access Denied');
        }
        return false;
    }

    //----------------------------------------------------------------------
    function addUserById($uid)
    {
        if ($this->customerId === null) {
            throw new \Exception('No Customer ID set, please call getCustomerPerPage before ...');
        }
        if ($this->customer['normal_be_users'] === '' || $this->customer['normal_be_users'] === null) {
            $this->customer['normal_be_users'] = $uid;
        } else {
            $this->customer['normal_be_users'] .= ',' . $uid;
        }


        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sitemgr_customer');
        $queryBuilder
            ->getRestrictions()
            ->removeAll();
        $queryBuilder
            ->update('tx_sitemgr_customer')
            ->where($queryBuilder->expr()->eq('uid', (int)$this->customerId));
        foreach ($this->customer as $key => $value) {
            $queryBuilder->set($key, $value);
        }
        $queryBuilder->execute();
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

}
