<?php

namespace KayStrobach\Sitemgr\Utilities;

use TYPO3\CMS\Core\Resource\StorageRepository;

class FileSystemUtility {
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;

	/**
	 * prepare objects
	 */
	public function __construct() {
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
	}

	/**
	 * entry point
	 *
	 * @param $folder
	 */
	public function ensureFolderExists($folder) {
		if(strpos($folder, ':') === FALSE) {
			$this->createInRealFileSystem($folder);
		} else {
			list($storage, $path) = explode(':', $folder, 2);
			$this->createInFalFileSystem($storage, $path);
		}

	}

	/**
	 * tries to create a folder via pre 6.0 api
	 *
	 * @param string $folder
	 */
	protected function createInRealFileSystem($folder) {
		\TYPO3\CMS\Core\Utility\GeneralUtility::mkdir($folder);
	}

	/**
	 * Tries to create a folder in FAL
	 *
	 * @param $storage
	 * @param $folder
	 * @throws Exception
	 * @throws \TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException
	 * @throws \TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException
	 */
	protected function createInFalFileSystem($storageUid, $folder) {
		/** @var \TYPO3\CMS\Core\Resource\StorageRepository $storageRepository */
		$storageRepository = $this->objectManager->get(StorageRepository::class);
		/** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
		$storage = $storageRepository->findByUid($storageUid);
		$newFolder = basename($folder);
		$parentFolder = dirname($folder);
		$storage->createFolder($newFolder, $storage->getFolder($parentFolder));
	}
}