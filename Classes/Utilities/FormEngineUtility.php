<?php

namespace KayStrobach\Sitemgr\Utilities;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Form\NodeFactory;
use TYPO3\CMS\Backend\Form\FormDataCompiler;
use TYPO3\CMS\Backend\Form\FormDataGroup\TcaDatabaseRecord;
use TYPO3\CMS\Backend\Form\FormResultCompiler;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class FormEngineUtility implements SingletonInterface
{
    /**
     * @param array $tca
     * @param array $fields
     * @return array
     */
    public function filterFields($tca, $fields)
    {
        $showRecordFieldList = $tca['interface']['showRecordFieldList'] ?? [];

        foreach($tca['columns'] as $column => $configuration) {
            if (!in_array($column, $fields)) {
                unset($tca['columns'][$column]);
            }
        }
        return $tca;
    }

    /**
     * @param string $table
     * @param int $recordUid
     * @param string $command
     * @param array $processedTca
     * @param array $databaseRow
     * @param array $fieldListToRender
     * @param int $effectivePid
     * @return string
     */
    public function renderForm($table, $recordUid, $command = 'edit', $processedTca = null, $databaseRow = null, $fieldListToRender = null, $effectivePid = 0)
    {
        /** @var TcaDatabaseRecord $formDataGroup */
        $formDataGroup = GeneralUtility::makeInstance(TcaDatabaseRecord::class);
        /** @var FormDataCompiler $formDataCompiler */
        $formDataCompiler = GeneralUtility::makeInstance(FormDataCompiler::class, $formDataGroup);
        /** @var NodeFactory $nodeFactory */
        $nodeFactory = GeneralUtility::makeInstance(NodeFactory::class);
        /** @var FormResultCompiler $formResultCompiler */
        $formResultCompiler = GeneralUtility::makeInstance(FormResultCompiler::class);
        $formDataCompilerInput = [
            'tableName' => $table,
            'vanillaUid' => $recordUid,
            'command' => $command,
        ];
        if ($databaseRow !== null) {
            $formDataCompilerInput['databaseRow'] = $databaseRow;
        }

        if ($processedTca !== null) {
            $formDataCompilerInput['processedTca'] = $processedTca;
        }

        $formData = $formDataCompiler->compile($formDataCompilerInput);
        $formData['renderType'] = 'outerWrapContainer';

        $formData['effectivePid'] = $effectivePid;

        if ($fieldListToRender !== null) {
            $formData['fieldListToRender'] = $fieldListToRender;
        }

        $formResult = $nodeFactory->create($formData)->render();
        $formResult['doSaveFieldName'] = 'doSave';
        $formResultCompiler->mergeResult($formResult);

        $pidFieldName = 'data[' . $table . '][' . $formData['databaseRow']['uid'] . '][pid]';

        $body = $formResultCompiler->addCssFiles();
        $body .= $this->compileForm($formResult['html'], $effectivePid, $pidFieldName);
        $body .= $formResultCompiler->printNeededJSFunctions();
        return $body;
    }

    /**
     * Put together the various elements (buttons, selectors, form) into a table
     *
     * @param string $editForm HTML form.
     * @return string Composite HTML
     */
    public function compileForm(string $editForm = '', int $pid = 0, string $formFieldName = null)
    {
        $formContent = $editForm;
        #$formContent = '
        #	<input type="hidden" name="returnUrl" value="' . htmlspecialchars($this->retUrl) . '" />
        #	<input type="hidden" name="viewUrl" value="' . htmlspecialchars($this->viewUrl) . '" />';
        #if ($this->returnNewPageId) {
        #    $formContent .= '<input type="hidden" name="returnNewPageId" value="1" />';
        #}
        #$formContent .= '<input type="hidden" name="popViewId" value="' . htmlspecialchars($this->viewId) . '" />';
        #if ($this->viewId_addParams) {
        #    $formContent .= '<input type="hidden" name="popViewId_addParams" value="' . htmlspecialchars($this->viewId_addParams) . '" />';
        #}
        if ($pid !== 0) {
            $formContent .= '<input type="hidden" name="' . $formFieldName . '" value="' . $pid . '" />';
        }
        $formContent .= '
			<input type="hidden" name="returnUrl" value="" />
			<input type="hidden" name="closeDoc" value="0" />
			<input type="hidden" name="doSave" value="0" />
			<input type="hidden" name="_serialNumber" value="' . md5(microtime()) . '" />
			<input type="hidden" name="_scrollPosition" value="" />';
        return $formContent;
    }

    /**
     * @param $data
     * @param array $allowedTables
     * @param array $allowedFields
     * @param bool $admin
     * @return DataHandler
     */
    public function handleData($data, $allowedTables = [], $allowedFields = [], $admin = false)
    {
        foreach($data as $tableName => &$rows) {
            if (!in_array($tableName, $allowedTables)) {
                unset($rows[$tableName]);
            } else {
                foreach ($rows as $item => $row) {
                    foreach($row as $fieldName => $fieldValues) {
                        if (!in_array($fieldName, $allowedFields)) {
                            unset($rows[$item][$fieldName]);
                        }
                    }
                }
            }
        }

        $altUser = clone $this->getBeUser();

        if ($admin) {
             $altUser->user['admin'] = 1;
        }

        /** @todo handle data security */
        /** @var DataHandler $tce */
        $tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);


        $tce->start(
            $data,
            [],
            $altUser
        );
        $tce->process_datamap();
        \TYPO3\CMS\Backend\Utility\BackendUtility::setUpdateSignal('updatePageTree');
        return $tce;
    }

    protected function getBeUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
