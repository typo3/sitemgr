<?php

namespace KayStrobach\Sitemgr\Utilities;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;

class PageTsConfigUtility
{
    public static function getForUserAndUid(BackendUserAuthentication $user, int $uid)
    {
        return array_replace_recursive(
            $user->getTSConfig(),
            BackendUtility::getPagesTSconfig($uid),
        );
    }

    public static function getForCurrentUserAndUid(int $uid)
    {
        return self::getForUserAndUid(
            $GLOBALS['BE_USER'],
            $uid
        );
    }
}
