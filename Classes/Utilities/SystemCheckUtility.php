<?php

namespace KayStrobach\Sitemgr\Utilities;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Crypto\PasswordHashing\ExtensionManagerConfigurationUtility;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class SystemCheckUtility implements SingletonInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    }

    public function getCheckResults()
    {
        $extensionConfiguration = $this->getExtensionConfiguration();
        $checks = [
            'rootCustomerPageEnforced' => isset($extensionConfiguration['customerPidPageTS']['value']) ? (bool)$extensionConfiguration['customerPidPageTS']['value'] : false,
            'rootCustomerPageDefinedInPageTS' => false,
            'enforceUserNamesWithPrefix' => isset($extensionConfiguration['forceBeUserPrefix']['value']) ? (bool)$extensionConfiguration['forceBeUserPrefix']['value'] : false,
            'shouldCreateUserHomePath' => (bool)strlen($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']),
            'userHomePath' => (string)$GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'],
            'userHomePathExists' => $this->checkIfFolderExists($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']),
            'shouldCreateGroupHomePath' => (bool)strlen($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']),
            'groupHomePath' => (string)$GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath'],
            'groupHomePathExists' => $this->checkIfFolderExists($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']),
            'newUserDefault' => [
                'group' => $this->getPageTsSettings('customer./createUser./group'),
            ],
            'customerPidPage' => $this->getPageTsSettings('customer./customerPidPage'),
        ];

        //throw new \Exception(json_encode($checks));

        return $checks;
    }

    public function hasErrors()
    {
        $checks = $this->getCheckResults();

        if (!$checks['shouldCreateUserHomePath']) {
            return true;
        }
        if (!$checks['userHomePathExists']) {
            return true;
        }
        if (!$checks['shouldCreateGroupHomePath']) {
            return true;
        }
        if (!$checks['groupHomePathExists']) {
            return true;
        }
        if (!$checks['newUserDefault']['group']) {
            return true;
        }
        return false;
    }

    protected function getExtensionConfiguration()
    {
        /** @var ExtensionConfiguration $configurationUtility */
        $configurationUtility = $this->objectManager->get(ExtensionConfiguration::class);
        return $configurationUtility->get('sitemgr');
    }

    protected function getPageTsSettings($path = '')
    {
        $id = 0;
        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
        }

        $result = PageTsConfigUtility::getForCurrentUserAndUid($id)['mod.']['web_txsitemgr.'] ?? null;

        if (is_array($result) && ($path !== '')) {
            return ArrayUtility::getValueByPath($result, $path, '/');
        }
        return $result;
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    protected function checkIfFolderExists($combinedIdentifier)
    {
        try {
            $resourceFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\ResourceFactory::class);
            $folder = $resourceFactory->getFolderObjectFromCombinedIdentifier($combinedIdentifier);
        } catch (InsufficientFolderAccessPermissionsException $exception) {
            return true;
        } catch (\Exception $exception) {
            return false;
        }
        if ($folder instanceof Folder) {
            return true;
        }
        return false;
    }
}
