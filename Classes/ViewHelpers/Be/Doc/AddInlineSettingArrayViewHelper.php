<?php
/*                                                                        *
 * This script belongs to the FLOW3 package "Fluid".                      *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License as published by the *
 * Free Software Foundation, either version 3 of the License, or (at your *
 * option) any later version.                                             *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU Lesser General Public       *
 * License along with the script.                                         *
 * If not, see http://www.gnu.org/licenses/lgpl.html                      *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

/**
 * ViewHelper which allows to add JS Files to a be container
 *
 * = Examples =
 *
 * <code title="Simple">
 *   <sitemgr:Be.Doc.AddInlineSettingArray namespace="sitemgr"  settings="{settings}" />
 * </code>
 * <output>
 * add js to header with the pagerenderer
 * </output>
 *
 *
 * @author Kay Strobach <typo3@kay-strobach.de>
 * @license http://www.gnu.org/copyleft/gpl.html
 */

namespace KayStrobach\Sitemgr\ViewHelpers\Be\Doc;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper;

class AddInlineSettingArrayViewHelper extends AbstractBackendViewHelper
{
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('namespace', 'string', 'Namespace use in JS', false, 'NULL');
        $this->registerArgument('settings', 'array', 'Array of settings', false, 'NULL');
    }

    public function render(): void
    {
        $namespace = $this->arguments['namespace'];
        $settings = $this->arguments['settings'];
        $pageRenderer = $this->getPageRenderer();
        if ($settings !== null && $namespace !== null) {
            $pageRenderer->addInlineSettingArray($namespace, $settings);
        }
    }


}
