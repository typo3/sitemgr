<?php
/*                                                                        *
 * This script belongs to the FLOW3 package "Fluid".                      *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License as published by the *
 * Free Software Foundation, either version 3 of the License, or (at your *
 * option) any later version.                                             *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU Lesser General Public       *
 * License along with the script.                                         *
 * If not, see http://www.gnu.org/licenses/lgpl.html                      *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

/**
 * ViewHelper which allows to add JS Files to a be container
 *
 * = Examples =
 *
 * <code title="Simple">
 *     <sitemgr:Be.Doc.AddJsFooterFile file="{module.jsFile}"/>
 * </code>
 * <output>
 * add js to header with the pagerenderer
 * </output>
 *
 *
 * @author Kay Strobach <typo3@kay-strobach.de>
 * @license http://www.gnu.org/copyleft/gpl.html
 */

namespace KayStrobach\Sitemgr\ViewHelpers\Be\Doc;

use TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper;

class AddJsFooterFileViewHelper extends AbstractBackendViewHelper
{
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('file', 'mixed', 'Custom JavaScript file to be loaded', false, 'NULL');
    }

    /**
     * add additional file
     *
     * @return void
     * @see template
     * @see t3lib_PageRenderer
     */
    public function render(): void
    {
        $file = $this->arguments['file'];
        $pageRenderer = $this->getPageRenderer();
        if ($file !== null) {
            if (is_array($file)) {
                foreach ($file as $singleFile) {
                    $pageRenderer->addJsFooterFile($singleFile);
                }
            } else {
                $pageRenderer->addJsFooterFile($file);
            }
        }
    }
}
