<?php

namespace  KayStrobach\Sitemgr\ViewHelpers\Be\Tree;

use KayStrobach\Sitemgr\Backend\Tree\View\BrowseTreeView;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Security\Cryptography\HashService;
use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper;

class BrowsableTreeViewHelper extends AbstractFormFieldViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'input';

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('pid', 'int', '', false, 0);
        $this->registerArgument('width', 'int', '', false, 250);
        $this->registerArgument('height', 'int', '', false, 350);
    }

    /**
     * @return string
     */
    public function render()
    {
        $pid = $this->arguments['pid'];
        $width = $this->arguments['width'];
        $height = $this->arguments['height'];
        $name = $this->getName();
        $this->registerFieldNameForFormTokenGeneration($name);
        $this->setRespectSubmittedDataValue(true);
        $this->tag->addAttribute('type', 'hidden');
        $this->tag->addAttribute('name', $name);
        $this->tag->addAttribute('value', $this->getValueAttribute());
        $this->registerFieldNameForFormTokenGeneration($name);
        $this->setRespectSubmittedDataValue(true);
        $this->addAdditionalIdentityPropertiesIfNeeded();
        $this->setErrorClassAttribute();
        /** @var BrowseTreeView $treeView */
        $treeView = GeneralUtility::makeInstance(BrowseTreeView::class);
        $treeView->init();
        $treeView->ext_IconMode = true;
        $treeUuid = md5($name);
        $this->tag->addAttribute('data-hiddenTreeField', $treeUuid);
        $treeView->setDataIdentifier($treeUuid);
        $treeView->thisScript = $this->getUri($pid);
        $treeView->MOUNTS = [$pid];
        $output = $treeView->getBrowsableTree();
        return  $this->tag->render() . '<div style="width:' . (int)$width . 'px;height:' . (int)$height . 'px;margin-left: 25px;" class="treeWrapper">' . $output . '</div>';
    }

    protected function getUri($pageUid)
    {
        $uriBuilder = $this->renderingContext->getControllerContext()->getUriBuilder();


        return $uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setArguments($uriBuilder->getRequest()->getArguments())
            ->uriFor(
                $this->renderingContext->getControllerContext()->getRequest()->getControllerActionName(),
                $this->renderingContext->getControllerContext()->getRequest()->getArguments(),
                $this->renderingContext->getControllerContext()->getRequest()->getControllerName(),
                $this->renderingContext->getControllerContext()->getRequest()->getControllerExtensionName(),
                $this->renderingContext->getControllerContext()->getRequest()->getPluginName()
            );
    }
}
