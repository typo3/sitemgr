<?php

return [
    'sitemgr:updateSites' => [
        'class' => \KayStrobach\Sitemgr\Command\UpdateTypo3SitesCommand::class,
    ],
    'sitemgr:fix:doublePrefixCommand' => [
        'class' => \KayStrobach\Sitemgr\Command\FixDoublePrefixCommand::class,
    ],
];
