<?php
declare(strict_types = 1);

return [
    \KayStrobach\Sitemgr\Domain\Model\Customer::class => [
        'tableName' => 'pages',
        'recordType' => ''
    ],
    \KayStrobach\Sitemgr\Domain\Model\Domain::class => [
        'tableName' => 'sys_domain',
        'recordType' => '',
        'properties' => [
            'domainName' => [
                'field' => 'domainName'
            ],
            'redirectTo' => [
                'field' => 'redirectTo'
            ],
            'redirectHttpStatusCode' => [
                'field' => 'redirectHttpStatusCode'
            ]
        ]
    ],
];
