<?php

call_user_func(
    function ($extKey, $table) {
        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:sitemgr_page_type',
                \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository::CUSTOMER_DOCTYPE,
                'EXT:' . $extKey . '/Resources/Public/Icons/pagetree-icon-sitemgr.svg'
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA'][$table],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository::CUSTOMER_DOCTYPE => 'apps-pagetree-sitemgr',
                    ],
                ],
            ]
        );
    },
    'sitemgr',
    'pages'
);

$GLOBALS['TCA']['pages']['columns'] = array_merge_recursive(
    $GLOBALS['TCA']['pages']['columns'],
    [
        'main_be_user' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.main_be_user',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'be_users',
                'size' => 1,
                'minitems' => 1,
                'maxitems' => 1,
                'prepend_tname' => false,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ]
        ],
        'admin_be_users' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.admin_be_users',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'be_users',
                'size' => 3,
                'autoSizeMax' => 15,
                'minitems' => 0,
                'maxitems' => 100,
                'prepend_tname' => false,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ]
        ],
        'normal_be_users' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.normal_be_users',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'be_users',
                'size' => 3,
                'autoSizeMax' => 15,
                'minitems' => 0,
                'maxitems' => 100,
                'prepend_tname' => false,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ]
        ],
        'be_groups' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.be_groups',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'be_groups',
                'size' => 1,
                'minitems' => 1,
                'maxitems' => 1,
                'prepend_tname' => false,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ]
        ]
    ]
);

$GLOBALS['TCA']['pages']['types'][\KayStrobach\Sitemgr\Domain\Repository\CustomerRepository::CUSTOMER_DOCTYPE] = [
    'showitem' =>
        '--div--;LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.div_required,title;;;;2-2-2,' .
            'doktype,alias,nav_title,subtitle,uris' .
        '--div--;LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.div_more,' .
            'main_be_user;;;;3-3-3,admin_be_users, normal_be_users,be_groups,' .
        '--div--;LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.div_contact,' .
            'tt_address_records'
];
