<?php

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('tt_address')) {
    $TCA['tx_sitemgr_customer']['columns']['tt_address_records'] = array(
        'exclude' => 0,
        'label' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:tx_sitemgr_customer.tt_address_records',
        'config' => array(
            'type' => 'inline',
            'foreign_table' => 'tt_address',
            'maxitems' => 10
        )
    );
}
