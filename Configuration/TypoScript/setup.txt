// Model/table mapping
config.tx_extbase {
    persistence.classes {
        KayStrobach\Sitemgr\Domain\Model\Customer {
            mapping {
                tableName = pages
                recordType = 
            }
        }
        KayStrobach\Sitemgr\Domain\Model\Domain {
            mapping {
                tableName = sys_domain
                recordType = 
                columns {
                    domainName.mapOnProperty = domainName
                    redirectTo.mapOnProperty = redirectTo
                    redirectHttpStatusCode.mapOnProperty = redirectHttpStatusCode
                }
            }
        }
    }
}


module.tx_sitemgr {
    persistence {
        storagePid = 0
    }
    view.widget {
        TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper {
            templateRootPath = EXT:sitemgr/Resources/Private/Templates/ViewHelpers
            templateRootPaths {
                10 = EXT:sitemgr/Resources/Private/Templates/ViewHelpers
            }
        }
    }
    settings {
        // This is a dummy entry. It is used in \TYPO3\CMS\Beuser\Controller\BackendUserController
        // to test that some TypoScript configuration is set.
        // This entry can be removed if extbase setup is made frontend TS independent
        // or if there are other settings set.
        dummy = foo
    }
}

[page["doktype"] == 157]
config >
page >
page = PAGE
page.10 = USER_INT
page.10 {
    userFunc = KayStrobach\Sitemgr\Frontend\RedirectToSubpage->redirect
}
[global]
