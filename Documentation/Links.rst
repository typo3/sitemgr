.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _links:

Links
-----

:TER:
	https://typo3.org/extensions/repository/view/sitemgr

:Bug Tracker:
	https://github.com/kaystrobach/TYPO3.sitemgr/issues

:Git Repository:
	https://github.com/kaystrobach/TYPO3.sitemgr

:Contact:
	`@kaystrobach <https://twitter.com/kaystrobach>`__
