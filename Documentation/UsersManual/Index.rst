﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _users-manual:

Users manual
============

Target group: **Users**, **Customer Administrators**

The customer module allows you to manage your users, either as TYPO3 admin, as Customer Manager or as Customer Administrator.

You always start in the overview page.

.. figure:: ../Images/UserManual/2017-05-04_21-03-40.png
	:width: 700px
	:alt: Backend view

	Customer overview page

	Customer overview page

On this page you are able to perform different actions on backend users related to your customer and also to configure
the domains related to your customer.
You are able to create new backend users and modify your rights.

.. figure:: ../Images/UserManual/2017-05-04_21-03-51.png
	:width: 700px
	:alt: Backend view

    	Default Backend view (caption of the image)

    	The Backend view of TYPO3 after the user has clicked on module "Page". (legend of the image)

Permission management is made very easy, we use access control lists instead of complex user / group / world permissions.
Additionally the module takes care of creating the needed database mount points for you.
This way it is a no brainer to add additional permissions to a user.

.. figure:: ../Images/UserManual/2017-05-04_21-04-00.png
	:width: 700px
	:alt: Backend view

    	Default Backend view (caption of the image)

    	The Backend view of TYPO3 after the user has clicked on module "Page". (legend of the image)
