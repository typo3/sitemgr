# What does it do?

EXT:sitemgr gives you the possibility to let customers manage their own users, without being admin.

This is possible, because users can get the right to manage an other group, which means, they can manage all members of the group.