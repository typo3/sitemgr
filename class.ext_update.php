<?php

class ext_update
{
    protected $records = [
        'cff12d60-46f9-11e7-9598-0800200c9a66' => [
            'pid' => 0,
            'tstamp' => 0,
            'title' => 'Sitemgr Manager',
            'hidden' => 0,
            'description' => 'Automatically created group',
            'deleted' => 0,
            'non_exclude_fields' => 'sys_domain:forced,sys_domain:hidden,sys_domain:redirectHttpStatusCode,sys_domain:prepend_params,pages:fe_group,pages:author,pages:description,pages:author_email,pages:nav_title,pages:starttime,pages:php_tree_stop,pages:endtime,pages:doktype',
            'custom_options' => 'tx_sitemgr_custom:permissions',
            'pagetypes_select' => '157',
            'tables_select' => 'sys_domain',
            'tables_modify' => 'sys_domain',
            'groupMods' => 'web_SitemgrTxSitemgrMod1',
            'sitemgr_uuid' => 'cff12d60-46f9-11e7-9598-0800200c9a66',
        ]
    ];

    /**
     * @return bool
     */
    public function access()
    {
        return true;
    }

    public function main()
    {
        $recordsCreated = false;
        foreach ($this->records as $key => $record) {
            if (!$this->recordExists($key)) {
                $this->createRecord($key, $record);
                $recordsCreated = true;
            }
        }

        if ($recordsCreated) {
            return 'Created default records';
        }

        return 'Nothing to do';
    }

    protected function recordExists($key)
    {
        $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('be_groups');
        $queryBuilder->getRestrictions()->removeAll()->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction::class));
        $queryBuilder->getRestrictions()->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction::class));
        $queryBuilder->select('*')->from('be_groups')->where($queryBuilder->expr()->eq('sitemgr_uuid', $queryBuilder->createNamedParameter($key)));
        return (bool)$queryBuilder->execute()->fetchAll();
    }

    protected function createRecord($key, $data)
    {
        $tce = $this->getDataHandler();
        $tce->start(
            [
                'be_groups' => [
                    'NEW9834589345' => $data
                ]
            ],
            []
        );
        $tce->process_datamap();
    }

    /**
     * @return \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected function getDataHandler()
    {
        $tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
        $tce->stripslashes_values = 0;
        return $tce;
    }
}