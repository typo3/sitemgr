<?php

########################################################################
# Extension Manager/Repository config file for ext "sitemgr".
#
# Auto generated 12-03-2013 08:57
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Sitemanager and Customer Management',
	'description' => 'Sitemanager and Customer Management made easy. With this extension you can create small admins´.',
	'category' => 0,
	'version' => '4.0.1',
	'state' => 1,
	'createDirs' => '',
	'clearCacheOnLoad' => true,
	'author' => 'Kay Strobach',
	'author_email' => 'typo3@kay-strobach.de',
	'author_company' => '',
	'constraints' => 	array (
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ),
);

?>
