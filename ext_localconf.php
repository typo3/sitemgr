<?php

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tstemplate.php']['includeStaticTypoScriptSourcesAtEnd'][]
    = \KayStrobach\Sitemgr\Hooks\T3libTstemplateIncludeStaticTypoScriptSourcesAtEndHook::class . '->main';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['sitemgrUpdateFromOldSitemgr']
= \KayStrobach\Sitemgr\Updates\UpdateFromOldSitemgr::class;
