<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}



/*******************************************************************************
 * Toolbar item
 */

// @todo migrate toolbar items to $GLOBALS['TYPO3_CONF_VARS']['BE']['toolbarItems']
$GLOBALS['TYPO3_CONF_VARS']['BE']['toolbarItems']['sitemgr_customerselector'] = \KayStrobach\Sitemgr\ToolbarItems\CustomerSelector\ToolbarItem::class;

/*******************************************************************************
 * Add Type icon for customer:
 */

// @todo replace spritemanager with new Icon API
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'sitemgr',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:sitemgr/Resources/Public/Icons/topmenu-icon-sitemgr.svg']
);
$iconRegistry->registerIcon(
    'apps-pagetree-sitemgr',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:sitemgr/Resources/Public/Icons/pagetree-icon-sitemgr.svg']
);
$iconRegistry->registerIcon(
    'contains-sitemgr',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:sitemgr/Resources/Public/Images/Modules/Customer/tabIcon.gif']
);
$iconRegistry->registerIcon(
    'sitemgr_module_icon',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:sitemgr/Resources/Public/Icons/Extension.svg']
);


call_user_func(
    function ($extKey, $dokType, $icon) {
        // Add new page type:
        $GLOBALS['PAGES_TYPES'][$dokType] = [
            'type' => 'web',
            'allowedTables' => '*',
        ];

        // Allow backend users to drag and drop the new page type:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
            'options.pageTree.doktypesToShowInNewPageDragArea := addToList(' . $dokType . ')'
        );
    },
    'sitemgr',
    \KayStrobach\Sitemgr\Domain\Repository\CustomerRepository::CUSTOMER_DOCTYPE,
    'contains-sitemgr'
);

/*******************************************************************************
 * Add extbase Module
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    'KayStrobach.sitemgr',
    'web',                    //Mainmodule
    'tx_sitemgr_mod1',        //Name
    '',                        //Position
    [                    //Controller
        \KayStrobach\Sitemgr\Controller\Backend\DefaultController::class => 'index',
        \KayStrobach\Sitemgr\Controller\Backend\CustomerController::class => 'index,edit,update,new,create',
        \KayStrobach\Sitemgr\Controller\Backend\BackendUserController::class => 'index,edit,editPassword,update,new,create',
        \KayStrobach\Sitemgr\Controller\Backend\SystemController::class => 'index',
        \KayStrobach\Sitemgr\Controller\Backend\AclController::class => 'index,edit,remove,new,create',
    ],
    [        //additional config
        'access' => 'user,group',
        'icon' => 'EXT:sitemgr/Resources/Public/Icons/Extension.svg',
        'labels' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_mod1.xml',
        'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement',
    ]
);

/*******************************************************************************
 * add context sensitive help
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_sitemgr_customer',
    'EXT:sitemgr/Resources/Private/Language/locallang_csh_tx_sitemgr_customer.xml'
);

/*******************************************************************************
 * reduce chooseable fe_user_groups to the ones defined below the customer root page
 */
/*if (TYPO3_MODE == 'BE') {
    // @todo use configurationobject
    $settings = unserialize($TYPO3_CONF_VARS['EXT']['extConf']['sitemgr']);
    if($settings['restrictFeUserAccessToCustomer']) {

        $config = array(
            'type'		=> 'select',
            'size'		=> 7,
            'maxitems'	=> 100,
            'items'		=>	array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.any_login', -2),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.usergroups', '--div--')
                ),
            'itemsProcFunc' => 'user_dbmissingthings->get_fe_user_groups'
        );


    }

}*/

$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['tx_sitemgr_custom'] = [
    'header' => 'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:sitemgr_grant',
    'items' => [
        'permissions' => [
            'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:sitemgr_grant_customer_manage',
            // Icon has been registered above
            'apps-pagetree-sitemgr',
            'LLL:EXT:sitemgr/Resources/Private/Language/locallang_db.xml:sitemgr_grant_customer_manage',
        ]
    ]
];



\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sitemgr_customer');

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = \KayStrobach\Sitemgr\Hooks\DataHandlerDataHook::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][] = \KayStrobach\Sitemgr\Hooks\DataHandlerCmdHook::class;


$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_befunc.php']['updateSignalHook']['SiteMgr::updateModule'] = \KayStrobach\Sitemgr\Hooks\BackendUtilityUpdateSignals::class . '->updateModule';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_befunc.php']['updateSignalHook']['SiteMgr::startCustomerEdit'] = \KayStrobach\Sitemgr\Hooks\BackendUtilityUpdateSignals::class . '->startCustomerEdit';
